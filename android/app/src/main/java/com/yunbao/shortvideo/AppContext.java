package com.yunbao.shortvideo;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tencent.rtmp.TXLiveBase;
import com.tencent.rtmp.TXLiveConstants;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.CommonAppContext;
import com.yunbao.common.utils.L;
import com.yunbao.im.utils.ImMessageUtil;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————

public class AppContext extends CommonAppContext {

    public static AppContext sInstance;
    private boolean mBeautyInited;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }


    public static void initSdk() {
        CommonAppConfig.getInstance().setLaunched(false);
        //腾讯云鉴权url
        String liveLicenceUrl = "http://license.vod2.myqcloud.com/license/v1/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/TXLiveSDK.licence";
        //腾讯云鉴权key
        String liveLicenceKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        TXLiveBase.getInstance().setLicence(CommonAppContext.sInstance, liveLicenceUrl, liveLicenceKey);
        TXLiveBase.setConsoleEnabled(true);
        TXLiveBase.setLogLevel(TXLiveConstants.LOG_LEVEL_DEBUG);
        L.setDeBug(BuildConfig.DEBUG);

        //初始化 ARouter
        if (BuildConfig.DEBUG) {
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(CommonAppContext.sInstance);
    }

    /**
     * 回到前台
     */
    @Override
    protected void onFrontGround() {
        wakeupTxIM();
    }

    /**
     * 唤醒腾讯IM
     */
    private void wakeupTxIM() {
        //初始化IM
        ImMessageUtil.getInstance().init();
        CommonAppContext.postDelayed(new Runnable() {
            @Override
            public void run() {
                ImMessageUtil.getInstance().loginImClient();
            }
        }, 500);
    }





}
