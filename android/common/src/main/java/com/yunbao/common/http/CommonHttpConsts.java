package com.yunbao.common.http;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class CommonHttpConsts {
    public static final String GET_CONFIG = "getConfig";
    public static final String SET_ATTENTION = "setAttention";
    public static final String GET_BALANCE = "getBalance";
    public static final String GET_ALI_ORDER = "getAliOrder";
    public static final String GET_WX_ORDER = "getWxOrder";
    public static final String DOWNLOAD_GIF = "downloadGif";
    public static final String DOWNLOAD_TX_IM_IMG = "downTxImImage";
    public static final String DOWNLOAD_TX_IM_VOICE = "downTxImVoice";
    public static final String GET_VIDEO_PRICE_TIP = "getVideoPriceTip";
    public static final String GET_COS_INFO = "getCosInfo";
    public static final String GET_BEAUTY_VALUE = "getBeautyValue";
    public static final String SET_BEAUTY_VALUE = "setBeautyValue";
    public static final String GET_ALI_ORDER_H5 = "getAliOrderH5";
    public static final String UPDATE_PAYMENT_STATUS = "updatePaymentStatus";
    public static final String CHECK_CONVERSATION = "checkConversation";
}
