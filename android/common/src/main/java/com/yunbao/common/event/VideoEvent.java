package com.yunbao.common.event;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class VideoEvent {

    private boolean isPlaying;


    public VideoEvent(boolean mIsPlaying) {
        this.isPlaying = mIsPlaying;
    }


    public boolean isPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(boolean mIsPlaying) {
        this.isPlaying =mIsPlaying;
    }

}
