package com.yunbao.common.interfaces;

/**
 * RecyclerView的Adapter点击事件
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public interface OnItemClickListener<T> {
    void onItemClick(T bean, int position);
}
