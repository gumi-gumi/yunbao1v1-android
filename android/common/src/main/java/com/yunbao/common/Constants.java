package com.yunbao.common;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class Constants {
    public static final String URL = "url";
    public static final String PAYLOAD = "payload";
    public static final String TO_UID = "toUid";
    public static final String POSITION = "position";
    public static final String TIP = "tip";
    public static final String FIRST_LOGIN = "firstLogin";
    public static final String SHOW_RECOMMEND = "showRecommend";
    public static final String USER_BEAN = "userBean";
    public static final String LIVE_UID = "liveUid";
    public static final String DIAMONDS = "云币";
    public static final String VOTES = "映票";
    public static final String PAY_ALI_NOT_ENABLE = "支付宝未接入";
    public static final String PAY_WX_NOT_ENABLE = "微信支付未接入";
    public static final String PAY_TYPE_ALI = "ali";
    public static final String PAY_TYPE_WX = "wx";
    public static final String PAY_TYPE_PAYPAL = "paypal";//paypal
    public static final String PAY_TYPE_ALI_H5 = "alih5";

    public static final String MATCH_PRICE = "matchPrice";
    public static final String MATCH_PRICE_VIP = "matchPriceVip";

    public static final String PAY_BUY_COIN_ALI_H5 = "Charge.getAliOrderh5";
    public static final String PAY_BUY_COIN_ALI = "Charge.getAliOrder";
    public static final String PAY_BUY_COIN_WX = "Charge.getWxOrder";

    public static final String PACKAGE_NAME_ALI = "com.eg.android.AlipayGphone";//支付宝的包名
    public static final String PACKAGE_NAME_WX = "com.tencent.mm";//微信的包名
    public static final String SELECT_IMAGE_PATH = "selectedImagePath";
    public static final String COPY_PREFIX = "copy://";

    public static final String GIF_GIFT_PREFIX = "gif_gift_";

    public static final String NOT_LOGIN_UID = "-9999";//未登录的uid

    //主播在线类型
    public static final int LINE_TYPE_OFF = 0;//离线
    public static final int LINE_TYPE_DISTURB = 1;//勿扰
    public static final int LINE_TYPE_CHAT = 2;//在聊
    public static final int LINE_TYPE_ON = 3;//在线


    //提现账号类型，1表示支付宝，2表示微信，3表示银行卡
    public static final int CASH_ACCOUNT_ALI = 1;
    public static final int CASH_ACCOUNT_WX = 2;
    public static final int CASH_ACCOUNT_BANK = 3;
    public static final String CASH_ACCOUNT_ID = "cashAccountID";
    public static final String CASH_ACCOUNT = "cashAccount";
    public static final String CASH_ACCOUNT_TYPE = "cashAccountType";

    public static final String VIDEO_PATH = "videoPath";
    public static final String VIDEO_MUSIC_ID = "videoMusicId";
    public static final String VIDEO_SAVE_TYPE = "videoSaveType";
    public static final int VIDEO_SAVE_PUB = 3;//仅发布


    public static final int MAIN_ME_GIFI_CAB = 5;//礼物柜
    public static final int MAIN_ME_VIDEO = 6;//视频接听
    public static final int MAIN_ME_VOICE = 7;//语音接听
    public static final int MAIN_ME_DISTURB = 8;//勿扰
    public static final int MAIN_ME_WALL = 11;//背景墙
    public static final int MAIN_ME_MY_ALBUM = 13;//我的相册
    public static final int AUTH_IMAGE_MAX_SIZE = 6;//MAX_SIZE 认证时候上传图片最大张数

    public static final String CHAT_TYPE = "chatType";//通话类型 1视频 2语音
    public static final byte CHAT_TYPE_VIDEO = 1;//通话类型 视频
    public static final byte CHAT_TYPE_VOICE = 2;//通话类型 语音
    public static final byte CHAT_TYPE_NONE = 0;//通话类型 全部


    public static final String CHAT_PARAM_AUD = "chatParamAud";
    public static final String CHAT_PARAM_ANC = "chatParamAnc";
    public static final String CHAT_PARAM_TYPE = "chatParamType";
    public static final String CHAT_SESSION_ID = "chatSessionId";
    public static final int CHAT_PARAM_TYPE_AUD = 1;
    public static final int CHAT_PARAM_TYPE_ANC = 2;


    public static final String CHAT_HANG_TYPE_WAITING = "0";//0等待中挂断
    public static final String CHAT_HANG_TYPE_WAIT_END = "1";//1等待结束后主播无响应挂断
    public static final String CHAT_HANG_TYPE_CHAT = "2";//2通话中挂断

    public static final String MAIN_SEX = "mainHomeSex";
    public static final byte MAIN_SEX_NONE = 0;


    public static final String PHOTO_BEAN = "photoBean";
    public static final String WALL_BEAN = "wallBean";
    public static final String WALL_IMAGE_SIZE = "wallImageSize";
    public static final String WALL_IS_VIDEO = "wallIsVideo";
    public static final String WALL_ACTION = "wallAction";
    public static final String WALL_OLD_ID = "wallOldId";
    /*7.22*/
    public static final String RANK_DEFAULT = "default_pos";

    public static final String UPLOAD_TYPE_AWS = "amazonaws.com";//已有链接的、使用亚马逊存储保存的文件链接  判断标识
    public static final String UPLOAD_TYPE_QINIU = "qiniu";//已有链接的、使用七牛存储保存的文件链接  判断标识
    public static final String UPLOAD_TYPE_PREFIX_AWS = "aws_";//使用亚马逊存储保存的文件链接  判断标识
    public static final String UPLOAD_TYPE_PREFIX_QINIU = "qiniu_";//使用七牛存储保存的文件链接  判断标识


    public static final long CHAT_WAIT_TIME = 60000;//接听时候的等待时间 1分钟
    public static final String CALL_TIME = "callTime";
}
