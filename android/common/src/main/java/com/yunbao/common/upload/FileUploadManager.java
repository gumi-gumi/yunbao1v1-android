package com.yunbao.common.upload;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.ToastUtil;

/**
 * 管理上传工具类型
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class FileUploadManager {
    private static FileUploadManager mInstance;

    public static FileUploadManager getInstance(){
        if (mInstance == null){
           synchronized (FileUploadManager.class){
               if (mInstance == null){
                   mInstance = new FileUploadManager();
               }
           }
        }
        return mInstance;
    }

    public void createUploadImpl(final Context context,final CommonCallback<UploadStrategy> commonCallback){
        CommonHttpUtil.getUploadCosInfo(new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0 && info.length > 0){
                    UploadInfoBean infoBean = JSON.parseObject(info[0],UploadInfoBean.class);
                    UploadStrategy uploadStrategy =  new UploadQnImpl(context,infoBean);
                    if (commonCallback!=null){
                        commonCallback.callback(uploadStrategy);
                    }
                }else {
                    if (commonCallback!=null){
                        commonCallback.callback(null);
                    }
                    ToastUtil.show(msg);
                }
            }

            @Override
            public void onError() {
                super.onError();
                if (commonCallback!=null){
                    commonCallback.callback(null);
                }
            }
        });
    }

}
