package com.yunbao.common.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;


import com.yunbao.common.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class FileUtil {

    /**
     * 把字符串保存成文件
     */
    public static void saveStringToFile(File dir, String content, String fileName) {
        PrintWriter writer = null;
        try {
            FileOutputStream os = new FileOutputStream(new File(dir, fileName));
            writer = new PrintWriter(os);
            writer.write(content);
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public static Bitmap getVideoThumbnail(String filePath) {
            MediaMetadataRetriever media =new MediaMetadataRetriever();
            media.setDataSource(filePath);
            Bitmap bitmap = media.getFrameAtTime();
            return bitmap;

    }

    public static void saveImageToGallery(Context context, Bitmap bmp) {
//        // 首先 创建文件夹
//        File appDir = new File(Environment.getExternalStorageDirectory(), "shy");
//        if (!appDir.exists()) {
//            appDir.mkdir();
//        }
//        //图片文件名称
//        String fileName = "shy_"+System.currentTimeMillis() + ".jpg";
//        File file = new File(appDir, fileName);
//        try {
//            FileOutputStream fos = new FileOutputStream(file);
//            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
//            fos.flush();
//            fos.close();
//        } catch (Exception e) {
//            Log.e("111",e.getMessage());
//            e.printStackTrace();
//        }

        MediaStore.Images.Media.insertImage(context.getContentResolver(), bmp, "title", "description");
       context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File("/sdcard/Boohee/image.jpg"))));
//        // 其次把文件插入到系统图库
//        String path = file.getAbsolutePath();
//        try {
//            MediaStore.Images.Media.insertImage(context.getContentResolver(), path, fileName, null);
//        } catch (FileNotFoundException e) {
//            Log.e("333",e.getMessage());
//            e.printStackTrace();
//        }
        // 最后通知图库更新
//        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        Uri uri = Uri.fromFile(file);
//        intent.setData(uri);
//        context.sendBroadcast(intent);
        ToastUtil.show(R.string.save_success);
    }
    public static Bitmap getBitmap(String netUrl) {
        URL url = null;
        try {
            url = new URL(netUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() == 200) {
                InputStream inputStream = conn.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File saveFile(Bitmap bmp)  {
        // 首先保存图片 创建文件夹
        File appDir = new File(Environment.getExternalStorageDirectory(), "shy");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        //图片文件名称
        String fileName = "shy_"+System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            Log.e("111",e.getMessage());
            e.printStackTrace();
        }
        return file;

    }

}
