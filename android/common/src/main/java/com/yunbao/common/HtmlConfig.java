package com.yunbao.common;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class HtmlConfig {

    //登录即代表同意服务和隐私条款
    public static final String LOGIN_PRIVCAY = CommonAppConfig.HOST + "/appapi/page/detail?id=1";
    //提现记录
    public static final String CASH_RECORD = CommonAppConfig.HOST + "/appapi/cash/index?";
    //支付宝充值回调地址
    public static final String ALI_PAY_COIN_URL = CommonAppConfig.HOST + "/Appapi/Pay/notify_ali";
    //钱包明细 支出
    public static final String WALLET_EXPAND = CommonAppConfig.HOST + "/appapi/record/expend";
    //钱包明细 收入
    public static final String WALLET_INCOME = CommonAppConfig.HOST + "/appapi/record/income";
    //充值协议
    public static final String CHARGE_PRIVCAY = CommonAppConfig.HOST + "/appapi/page/detail?id=3";
    //关于我们
    public static final String ABOUT_US = CommonAppConfig.HOST + "/appapi/page/lists";
    //隐私政策
    public static final String PRIVACY = CommonAppConfig.HOST + "/appapi/page/detail?id=8";
    //服务协议
    public static final String SERVICE = CommonAppConfig.HOST + "/appapi/page/detail?id=9";

}
