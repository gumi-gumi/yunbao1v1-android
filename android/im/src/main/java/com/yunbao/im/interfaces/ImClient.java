package com.yunbao.im.interfaces;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public interface ImClient {

    /**
     * 初始化
     */
    void init();

    /**
     * 登录IM
     */
    void loginImClient();

    /**
     * 登出IM
     */
    void logoutImClient();


    /**
     * 发送自定义消息
     *
     * @param toUid
     * @param data  要发送的数据
     * @return
     */
    void sendCustomMessage(String toUid, String data, boolean save);



}
