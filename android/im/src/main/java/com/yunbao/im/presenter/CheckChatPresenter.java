package com.yunbao.im.presenter;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.bean.ChatAudienceParam;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.im.R;
import com.yunbao.im.http.ImHttpConsts;
import com.yunbao.im.http.ImHttpUtil;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class CheckChatPresenter {

    private HttpCallback mAudToAncCallback;//观众邀请主播的回调
    private Context mContext;

    public CheckChatPresenter(Context context) {
        mContext = context;
    }

    /**
     * 观众向主播发起通话邀请
     *
     * @param liveUid    主播的uid
     * @param chatType   通话类型
     * @param toUserBean 主播的userBean
     */
    public void chatAudToAncStart(String liveUid, final int chatType, final UserBean toUserBean) {
        if (TextUtils.isEmpty(liveUid) || toUserBean == null) {
            return;
        }
        if (mAudToAncCallback == null) {
            mAudToAncCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        if (info.length > 0) {
                            JSONObject obj = JSON.parseObject(info[0]);
                            ChatAudienceParam param = new ChatAudienceParam();
                            param.setAnchorID(toUserBean.getId());
                            param.setAnchorName(toUserBean.getUserNiceName());
                            param.setAnchorAvatar(toUserBean.getAvatar());
                            param.setAnchorLevel(toUserBean.getLevelAnchor());
                            param.setSessionId(obj.getString("showid"));
                            param.setAudiencePlayUrl(obj.getString("pull"));
                            param.setAudiencePushUrl(obj.getString("push"));
                            param.setAnchorPrice(obj.getString("total"));
                            param.setChatType(obj.getIntValue("type"));
                            param.setAudienceActive(true);
                            RouteUtil.forwardAudienceActivity(param);
                        }
                    } else if (code == 800) {
                        if (mContext != null) {
                            new DialogUitl.Builder(mContext)
                                    .setContent(msg)
                                    .setConfrimString(WordUtil.getString(R.string.chat_subcribe))
                                    .setCancelable(true)
                                    .setClickCallback(new DialogUitl.SimpleCallback() {
                                        @Override
                                        public void onConfirmClick(Dialog dialog, String content) {
                                            ImHttpUtil.audSubscribeAnc(toUserBean.getId(), chatType);
                                            ToastUtil.show(R.string.chat_subcribe_success);
                                        }
                                    })
                                    .build()
                                    .show();

                        }
                    } else {
                        ToastUtil.show(msg);
                    }
                }
            };
        }
        ImHttpUtil.chatAudToAncStart(liveUid, chatType, mAudToAncCallback);
    }


    public void cancel() {
        ImHttpUtil.cancel(ImHttpConsts.CHAT_AUD_TO_ANC_START);
    }

}
