package com.yunbao.im.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.http.HttpClient;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.MD5Util;
import com.yunbao.common.utils.SpUtil;
import com.yunbao.common.utils.StringUtil;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ImHttpUtil {

    /**
     * 取消网络请求
     */
    public static void cancel(String tag) {
        HttpClient.getInstance().cancel(tag);
    }


    /**
     * 1v1更新用户在线状态
     * @param onlineStatus 状态，0离线，1勿扰，2在聊，3在线
     */
    public static void setOnlineStatus(int onlineStatus) {
        HttpClient.getInstance().get("Login.setUserOnline", "setUserOnline")
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("online", onlineStatus)
                .execute(CommonHttpUtil.NO_CALLBACK);
    }


    /**
     * 私信聊天页面用于获取用户信息
     */
    public static void getImUserInfo(String uids, HttpCallback callback) {
        HttpClient.getInstance().get("Im.GetMultiInfo", ImHttpConsts.GET_IM_USER_INFO)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("uids", uids)
                .execute(callback);
    }

    /**
     * 获取系统消息列表
     */
    public static void getSystemMessageList(int p, HttpCallback callback) {
        HttpClient.getInstance().get("Im.GetSysNotice", ImHttpConsts.GET_SYSTEM_MESSAGE_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("p", p)
                .execute(callback);
    }

    /**
     * 判断自己有没有被对方拉黑，聊天的时候用到
     */
    public static void checkIm(String touid, HttpCallback callback) {
        HttpClient.getInstance().get("Im.Check", ImHttpConsts.CHECK_IM)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("touid", touid)
                .execute(callback);
    }

    /**
     * 私信聊天敏感词替换
     */
    public static void checkIMMsg(String content, HttpCallback callback) {
        HttpClient.getInstance().get("Im.checkIMMsg", ImHttpConsts.CHECK_IMMSG)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("content", content)
                .execute(callback);
    }


    /**
     * 付费发送消息
     */
    public static void chargeSendIm(HttpCallback callback) {
        HttpClient.getInstance().get("Im.BuyIm", ImHttpConsts.CHARGE_SEND_IM)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


    /**
     * 获取礼物列表，同时会返回剩余的钱
     */
    public static void getGiftList(HttpCallback callback) {
        HttpClient.getInstance().get("Gift.getGiftList", ImHttpConsts.GET_GIFT_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }

    /**
     * 观众给主播送礼物
     */
    public static void sendGift(String liveUid, String sessionId, int giftId, String giftCount, HttpCallback callback) {
        HttpClient.getInstance().get("Gift.SendGift", ImHttpConsts.SEND_GIFT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("liveuid", liveUid)
                .params("showid", sessionId)
                .params("giftid", giftId)
                .params("nums", giftCount)
                .execute(callback);
    }


    /**
     * 通话时候 用于检测两个用户间关系
     */
    public static void checkChatStatus(String toUid, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("token=", token, "&touid=", toUid, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.Checkstatus", ImHttpConsts.CHECK_CHAT_STATUS)
                .params("uid", uid)
                .params("token", token)
                .params("touid", toUid)
                .params("sign", sign)
                .execute(callback);
    }


    /**
     * 观众向主播发起通话邀请，检测主播状态，同时获取自己的推拉流地址
     *
     * @param liveUid  主播的 id
     * @param type     通话类型
     * @param callback
     */
    public static void chatAudToAncStart(String liveUid, int type, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("liveuid=", liveUid, "&token=", token, "&type=", String.valueOf(type), "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.Checklive", ImHttpConsts.CHAT_AUD_TO_ANC_START)
                .params("uid", uid)
                .params("token", token)
                .params("liveuid", liveUid)
                .params("type", type)
                .params("sign", sign)
                .execute(callback);
    }

    /**
     * 观众预约主播
     *
     * @param liveUid 主播的uid
     */
    public static void audSubscribeAnc(String liveUid, int type) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("liveuid=", liveUid, "&token=", token, "&type=", String.valueOf(type), "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Subscribe.SetSubscribe", ImHttpConsts.AUD_SUBSCRIBE_ANC)
                .params("uid", uid)
                .params("token", token)
                .params("liveuid", liveUid)
                .params("type", type)
                .params("sign", sign)
                .execute(CommonHttpUtil.NO_CALLBACK);
    }

    /**
     * 主播向观众发起通话邀请，检测观众状态，同时获取自己的推拉流地址
     */
    public static void chatAncToAudStart2(String toUid, int type, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("token=", token, "&touid=", toUid, "&type=", String.valueOf(type), "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.AnchorLaunch", ImHttpConsts.CHAT_ANC_TO_AUD_START_2)
                .params("uid", uid)
                .params("token", token)
                .params("touid", toUid)
                .params("type", type)
                .params("sign", sign)
                .execute(callback);
    }


    /**
     * 获取用户信息
     */
    public static void getBaseInfo(String uid, String token, final CommonCallback<UserBean> commonCallback) {
        HttpClient.getInstance().get("User.getBaseInfo", ImHttpConsts.GET_BASE_INFO)
                .params("uid", uid)
                .params("token", token)
                .execute(new HttpCallback() {
                    @Override
                    public void onSuccess(int code, String msg, String[] info) {
                        if (code == 0 && info.length > 0) {
                            JSONObject obj = JSON.parseObject(info[0]);
                            UserBean bean = JSON.toJavaObject(obj, UserBean.class);
                            CommonAppConfig appConfig = CommonAppConfig.getInstance();
                            appConfig.setUserBean(bean);
                            appConfig.setPriceVideo(obj.getString("video_value"));
                            appConfig.setUserSwitchVideo(obj.getIntValue("isvideo") == 1);
                            appConfig.setUserItemList(obj.getString("list"));
                            SpUtil.getInstance().setStringValue(SpUtil.USER_INFO, info[0]);
                            if (commonCallback != null) {
                                commonCallback.callback(bean);
                            }
                        }
                    }

                    @Override
                    public void onError() {
                        if (commonCallback != null) {
                            commonCallback.callback(null);
                        }
                    }
                });
    }


    /**
     * 获取用户信息
     */
    public static void getBaseInfo(CommonCallback<UserBean> commonCallback) {
        getBaseInfo(CommonAppConfig.getInstance().getUid(),
                CommonAppConfig.getInstance().getToken(),
                commonCallback);
    }

}
