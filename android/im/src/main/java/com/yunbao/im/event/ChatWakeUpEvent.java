package com.yunbao.im.event;

import com.yunbao.common.bean.ChatAnchorParam;
import com.yunbao.common.bean.ChatAudienceParam;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatWakeUpEvent {
    private ChatAudienceParam mAudienceParam;
    private ChatAnchorParam mAnchorParam;
    private boolean mSelfAnchor;//自己是不是主播

    public ChatWakeUpEvent(ChatAudienceParam audienceParam, ChatAnchorParam anchorParam, boolean selfAnchor) {
        mAudienceParam = audienceParam;
        mAnchorParam = anchorParam;
        mSelfAnchor = selfAnchor;
    }

    public ChatAudienceParam getAudienceParam() {
        return mAudienceParam;
    }

    public ChatAnchorParam getAnchorParam() {
        return mAnchorParam;
    }

    public boolean isSelfAnchor() {
        return mSelfAnchor;
    }
}
