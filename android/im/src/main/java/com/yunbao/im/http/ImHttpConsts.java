package com.yunbao.im.http;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ImHttpConsts {
    public static final String GET_IM_USER_INFO = "getImUserInfo";
    public static final String GET_SYSTEM_MESSAGE_LIST = "getSystemMessageList";
    public static final String CHECK_IM = "checkIm";
    public static final String CHECK_IMMSG = "checkIMMsg";
    public static final String CHARGE_SEND_IM = "chargeSendIm";
    public static final String GET_GIFT_LIST = "getGiftList";
    public static final String SEND_GIFT = "sendGift";
    public static final String CHECK_CHAT_STATUS = "checkChatStatus";
    public static final String CHAT_AUD_TO_ANC_START = "chatAudToAncStart";
    public static final String AUD_SUBSCRIBE_ANC = "audSubscribeAnc";
    public static final String CHAT_ANC_TO_AUD_START_2 = "chatAncToAudStart2";
    public static final String GET_BASE_INFO = "getBaseInfo";
}
