package com.yunbao.im.utils;

import com.yunbao.common.utils.L;
import com.yunbao.im.interfaces.ImClient;

/**
 * 极光IM注册、登陆等功能
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ImMessageUtil {

    private static ImMessageUtil sInstance;
    private ImClient mImClient;

    private ImMessageUtil() {
        mImClient = new TxImMessageUtil();
    }

    public static ImMessageUtil getInstance() {
        if (sInstance == null) {
            synchronized (ImMessageUtil.class) {
                if (sInstance == null) {
                    sInstance = new ImMessageUtil();
                }
            }
        }
        return sInstance;
    }


    public void init() {
        if (mImClient != null) {
            mImClient.init();
        }
    }

    /**
     * 登录极光IM
     */
    public void loginImClient() {
        if (mImClient != null) {
            mImClient.loginImClient();
        }
    }


    /**
     * 登出IM
     */
    public void logoutImClient() {
        if (mImClient != null) {
            mImClient.logoutImClient();
        }
    }


    /**
     * 发送自定义消息
     */
    public void sendCustomMessage(String toUid, String data) {
        sendCustomMessage(toUid, data, true);
    }

    /**
     * 发送自定义消息
     */
    public void sendCustomMessage(String toUid, String data, boolean save) {
        if (mImClient != null) {
            mImClient.sendCustomMessage(toUid, data, save);
        }
        L.e("ChatIM-----发送--toUid--> " + toUid + "----> " + data);
    }

}
