package com.yunbao.im.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.yunbao.common.dialog.AbsDialogFragment;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.im.R;
import com.yunbao.im.activity.ChooseImageActivity;

import java.io.File;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatImagePrevDialog extends AbsDialogFragment implements View.OnClickListener {

    private String mFilePath;

    @Override
    protected int getLayoutId() {
        return R.layout.view_chat_image_prev;
    }

    @Override
    protected int getDialogStyle() {
        return R.style.dialog2;
    }

    @Override
    protected boolean canCancel() {
        return true;
    }

    @Override
    protected void setWindowAttributes(Window window) {
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(params);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRootView.findViewById(R.id.btn_close).setOnClickListener(this);
        mRootView.findViewById(R.id.btn_use).setOnClickListener(this);
        ImageView imageView = mRootView.findViewById(R.id.img);
        ImgLoader.display(mContext, new File(mFilePath), imageView);
    }


    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_close) {
            dismiss();
        } else if (i == R.id.btn_use) {
            dismiss();
            if (mContext != null && mContext instanceof ChooseImageActivity) {
                ((ChooseImageActivity) mContext).sendImage(mFilePath);
            }

        }
    }
}
