package com.yunbao.one.bean;

/**
 * 0/12.
 */


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class LiveEnterRoomBean {

    private LiveUserGiftBean mUserBean;
    private LiveChatBean mLiveChatBean;

    public LiveEnterRoomBean(LiveUserGiftBean userBean, LiveChatBean liveChatBean) {
        mUserBean = userBean;
        mLiveChatBean = liveChatBean;
    }


    public LiveUserGiftBean getUserBean() {
        return mUserBean;
    }

    public void setUserBean(LiveUserGiftBean userBean) {
        mUserBean = userBean;
    }

    public LiveChatBean getLiveChatBean() {
        return mLiveChatBean;
    }

    public void setLiveChatBean(LiveChatBean liveChatBean) {
        mLiveChatBean = liveChatBean;
    }
}
