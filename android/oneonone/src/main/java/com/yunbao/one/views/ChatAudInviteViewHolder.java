package com.yunbao.one.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.Constants;
import com.yunbao.common.bean.ChatAudienceParam;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.im.utils.ChatLiveImUtil;
import com.yunbao.one.R;
import com.yunbao.one.activity.ChatAudienceActivity;
import com.yunbao.one.activity.ChatBaseActivity;

/**
 * 2019/4/19.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatAudInviteViewHolder extends AbsChatInviteViewHolder implements View.OnClickListener {

    private ImageView mAvatar;
    private TextView mName;
    private TextView mPrice;
    private TextView mTip;
    private View mBtnCancel;
    private View mBtnAccept;
    private ImageView mImgAccept;

    public ChatAudInviteViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_chat_invite_aud;
    }

    @Override
    public void init() {
        mAvatar = (ImageView) findViewById(R.id.avatar);
        mName = (TextView) findViewById(R.id.name);
        mPrice = (TextView) findViewById(R.id.price);
        mTip = (TextView) findViewById(R.id.tip);
        mBtnCancel = findViewById(R.id.btn_cancel);
        mBtnAccept = findViewById(R.id.btn_accept);
        mBtnCancel.setOnClickListener(this);
        mBtnAccept.setOnClickListener(this);
        mImgAccept = (ImageView) findViewById(R.id.img_accept);
    }


    @Override
    public void onClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.btn_cancel) {
            ((ChatAudienceActivity) mContext).onBackPressed();
        } else if (i == R.id.btn_accept) {
            ((ChatAudienceActivity) mContext).accpetChat();
        }
    }

    /**
     * 观众向主播发起通话邀请
     *
     * @param param     主播的信息
     * @param price     通话价格
     * @param sessionId 通话会话ID
     * @param chatType  通话类型
     * @param active    是否是观众主动发起的
     */
    public void showData(ChatAudienceParam param, String price, String sessionId, int chatType, boolean active) {
        mImgAccept.setImageResource(chatType == Constants.CHAT_TYPE_VIDEO ? R.mipmap.o_chat_invite_accept_video : R.mipmap.o_chat_invite_accept_voice);
        ImgLoader.display(mContext, param.getAnchorAvatar(), mAvatar);
        mName.setText(param.getAnchorName());
        mPrice.setText(price);
        if (active) {
            mTip.setText(R.string.chat_invite_tip_1);
            ChatLiveImUtil.chatAudToAncStart(param.getAnchorID(), sessionId, chatType);
            playRingMusic();
            ((ChatBaseActivity) mContext).startWait();
        } else {
            mTip.setText(chatType == Constants.CHAT_TYPE_VIDEO ? R.string.chat_invite_tip_2 : R.string.chat_invite_tip_3);
            mBtnAccept.setVisibility(View.VISIBLE);
            playRingMusic();
            ((ChatBaseActivity) mContext).startRing();
        }
    }


}
