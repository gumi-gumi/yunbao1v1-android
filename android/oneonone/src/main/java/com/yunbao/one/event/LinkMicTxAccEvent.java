package com.yunbao.one.event;

/**
 * 2019/3/25.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class LinkMicTxAccEvent {

    private boolean mLinkMic;

    public LinkMicTxAccEvent(boolean linkMic) {
        mLinkMic = linkMic;
    }

    public boolean isLinkMic() {
        return mLinkMic;
    }
}
