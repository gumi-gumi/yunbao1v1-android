package com.yunbao.one.dialog;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.yunbao.common.Constants;
import com.yunbao.common.bean.CoinPayBean;
import com.yunbao.common.dialog.AbsDialogFragment;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.common.utils.DpUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.one.R;
import com.yunbao.one.adapter.ChatChargePayAdapter;

import java.util.List;

/**
 * 2019/4/22.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatChargePayDialogFragment extends AbsDialogFragment implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private List<CoinPayBean> mPayList;
    private ChatChargePayAdapter mAdapter;
    private String mCoinString;
    private String mMoneyString;
    private ActionListener mActionListener;
    private TextView tv_symbol;
    private String mMoneySymbol;

    private String mPaypalCoinString;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_chat_charge_pay;
    }

    @Override
    protected int getDialogStyle() {
        return R.style.dialog;
    }

    @Override
    protected boolean canCancel() {
        return true;
    }

    @Override
    protected void setWindowAttributes(Window window) {
        window.setWindowAnimations(R.style.bottomToTopAnim);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = DpUtil.dp2px(310);
        params.height = DpUtil.dp2px(330);
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findViewById(R.id.btn_charge).setOnClickListener(this);
        findViewById(R.id.btn_close).setOnClickListener(this);
        tv_symbol = (TextView) findViewById(R.id.tv_symbol);
        final TextView coin = (TextView) findViewById(R.id.coin);
        TextView money = (TextView) findViewById(R.id.money);
        coin.setText(mCoinString);
        money.setText(mMoneyString);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if (mPayList != null) {
            mAdapter = new ChatChargePayAdapter(mContext, mPayList);
            mAdapter.setOnItemClickListener(new OnItemClickListener<CoinPayBean>() {
                @Override
                public void onItemClick(CoinPayBean bean, int position) {
                    if (tv_symbol != null) {
                        if (Constants.PAY_TYPE_PAYPAL.equals(bean.getId())) {
                            mMoneySymbol = WordUtil.getString(R.string.money_symbol_dollar);
                            coin.setText(mPaypalCoinString);
                        } else {
                            mMoneySymbol = WordUtil.getString(R.string.money_symbol);
                            coin.setText(mCoinString);
                        }
                        tv_symbol.setText(mMoneySymbol);
                    }
                }
            });
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_close) {
            dismiss();
        } else if (i == R.id.btn_charge) {
            charge();
        }
    }

    private void charge() {
        if (mAdapter != null && mActionListener != null) {
            mActionListener.onChargeClick(mAdapter.getCheckedPayType());
            dismiss();
        }
    }


    public void setPayList(List<CoinPayBean> payList) {
        for (int i = 0, size = payList.size(); i < size; i++) {
            payList.get(i).setChecked(i == 0);
        }
        mPayList = payList;
    }

    public void setCoinString(String coinString, String coinPaypal) {
        mCoinString = coinString;
        mPaypalCoinString = coinPaypal;
    }

    public void setMoneyString(String moneyString) {
        mMoneyString = moneyString;
    }

    @Override
    public void onDestroy() {
        mActionListener = null;
        super.onDestroy();
    }

    public interface ActionListener {
        void onChargeClick(String payType);
    }


    public void setActionListener(ActionListener actionListener) {
        mActionListener = actionListener;
    }
}
