package com.yunbao.one.http;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.http.HttpClient;
import com.yunbao.common.utils.MD5Util;
import com.yunbao.common.utils.StringUtil;

/**
 * 2019/3/21.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class OneHttpUtil {

    /**
     * 取消网络请求
     */
    public static void cancel(String tag) {
        HttpClient.getInstance().cancel(tag);
    }


    /**
     * 主播向观众赴约，检测观众状态，同时获取自己的推拉流地址
     *
     * @param subscribeId 预约的 id
     * @param callback
     */
    public static void chatAncToAudStart(String subscribeId, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("subscribeid=", subscribeId, "&token=", token, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.ToAppointment", OneHttpConsts.CHAT_ANC_TO_AUD_START)
                .params("uid", uid)
                .params("token", token)
                .params("subscribeid", subscribeId)
                .params("sign", sign)
                .execute(callback);
    }


    /**
     * 观众挂断通话
     *
     * @param liveUid       主播的 id
     * @param chatSessionId 通话的会话id
     * @param hangType      挂断类型 0等待中挂断 1等待结束后主播无响应挂断 2通话中挂断
     */
    public static void chatAudienceHangUp(String liveUid, String chatSessionId, String hangType, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("liveuid=", liveUid, "&showid=", chatSessionId, "&token=", token, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.UserHang", OneHttpConsts.CHAT_AUDIENCE_HANG_UP)
                .params("uid", uid)
                .params("token", token)
                .params("liveuid", liveUid)
                .params("showid", chatSessionId)
                .params("hangtype", hangType)
                .params("sign", sign)
                .execute(callback != null ? callback : CommonHttpUtil.NO_CALLBACK);

    }


    /**
     * 主播挂断通话
     *
     * @param touid         通话对象的 id
     * @param chatSessionId 通话的会话id
     */
    public static void chatAnchorHangUp(String touid, String chatSessionId, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("showid=", chatSessionId, "&token=", token, "&touid=", touid, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.AnchorHang", OneHttpConsts.CHAT_ANCHOR_HANG_UP)
                .params("uid", uid)
                .params("token", token)
                .params("touid", touid)
                .params("showid", chatSessionId)
                .params("sign", sign)
                .execute(callback != null ? callback : CommonHttpUtil.NO_CALLBACK);

    }

    /**
     * 主播同意通话
     *
     * @param touid         通话对象的 id
     * @param chatSessionId 通话的会话id
     */
    public static void chatAnchorAccpet(String touid, String chatSessionId, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("showid=", chatSessionId, "&token=", token, "&touid=", touid, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.AnchorAnswer", OneHttpConsts.CHAT_ANCHOR_ACCPET)
                .params("uid", uid)
                .params("token", token)
                .params("touid", touid)
                .params("showid", chatSessionId)
                .params("sign", sign)
                .execute(callback);

    }

    /**
     * 观众同意通话
     *
     * @param liveUid       主播的 id
     * @param chatSessionId 通话的会话id
     */
    public static void chatAudienceAccpet(String liveUid, String chatSessionId, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String token = CommonAppConfig.getInstance().getToken();
        String sign = MD5Util.getMD5(StringUtil.contact("liveuid=", liveUid, "&showid=", chatSessionId, "&token=", token, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Live.UserAnswer", OneHttpConsts.CHAT_AUDIENCE_ACCPET)
                .params("uid", uid)
                .params("token", token)
                .params("liveuid", liveUid)
                .params("showid", chatSessionId)
                .params("sign", sign)
                .execute(callback);

    }


    /**
     * 通话结束时候获取评价标签列表
     */
    public static void getChatEvaList(HttpCallback callback) {
        HttpClient.getInstance().get("Label.GetEvaluate", OneHttpConsts.GET_CHAT_EVA_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }

    /**
     * 通话结束时观众对主播进行评价
     */
    public static void setChatEvaList(String liveUid, String evaIds) {
        HttpClient.getInstance().get("Label.SetEvaluate", OneHttpConsts.SET_CHAT_EVA_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("liveuid", liveUid)
                .params("evaluateids", evaIds)
                .execute(CommonHttpUtil.NO_CALLBACK);
    }



    /**
     * 计时收费的时候，观众每隔一分钟请求这个接口进行扣费
     *
     * @param liveUid 主播的uid
     */
    public static void timeCharge(String liveUid, String sessionId, HttpCallback callback) {
        HttpClient.getInstance().get("Live.TimeCharge", OneHttpConsts.TIME_CHARGE)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("liveuid", liveUid)
                .params("showid", sessionId)
                .execute(callback);
    }


    /**
     * 我预约的主播列表
     */
    public static void getMySubscribeList(int p, HttpCallback callback) {
        HttpClient.getInstance().get("Subscribe.GetMeto", OneHttpConsts.GET_MY_SUBCRIBE_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("p", p)
                .execute(callback);
    }

    /**
     * 预约我的列表
     */
    public static void getSubscribeMeList(int p, HttpCallback callback) {
        HttpClient.getInstance().get("Subscribe.GetTome", OneHttpConsts.GET_SUBCRIBE_ME_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("p", p)
                .execute(callback);
    }

    /**
     * 获取预约的个数
     */
    public static void getSubscribeNums(HttpCallback callback) {
        HttpClient.getInstance().get("Subscribe.GetSubscribeNums", OneHttpConsts.GET_SUBSCRIBE_NUMS)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


    /**
     * 观众在匹配前进行检测
     */
    public static void matchCheck(int type, HttpCallback callback) {
        HttpClient.getInstance().get("Match.Check", OneHttpConsts.MATCH_CHECK)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("type", type)
                .execute(callback);
    }


    /**
     * 获取匹配信息
     */
    public static void getMatchInfo(HttpCallback callback) {
        HttpClient.getInstance().get("Match.GetMatch", OneHttpConsts.GET_MATCH_INFO)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


    /**
     * 主播匹配
     */
    public static void matchAnchor(int type, HttpCallback callback) {
        HttpClient.getInstance().get("Match.AnchorMatch", OneHttpConsts.MATCH_ANCHOR)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("type", type)
                .execute(callback);
    }


    /**
     * 主播取消匹配
     */
    public static void matchAnchorCancel() {
        HttpClient.getInstance().get("Match.AnchorCancel", OneHttpConsts.MATCH_ANCHOR_CANCEL)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(CommonHttpUtil.NO_CALLBACK);
    }


    /**
     * 用户匹配
     */
    public static void matchAudience(int type, HttpCallback callback) {
        HttpClient.getInstance().get("Match.UserMatch", OneHttpConsts.MATCH_AUDIENCE)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("type", type)
                .execute(callback);
    }

    /**
     * 用户取消匹配
     */
    public static void matchAudienceCancel() {
        HttpClient.getInstance().get("Match.UserCancel", OneHttpConsts.MATCH_AUDIENCE_CANCEL)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(CommonHttpUtil.NO_CALLBACK);
    }


    /**
     * 主播认证
     */
    public static void setAuthorAuth(String thumb,
                                     String backwall,
                                     String video,
                                     String video_thumb,HttpCallback callback) {
        HttpClient.getInstance().get("Auth.setAuthorAuth", OneHttpConsts.SET_AUTHOR_AUTH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("thumb", thumb)
                .params("backwall", backwall)
                .params("video", video)
                .params("video_thumb", video_thumb)
                .execute(callback);
    }




}
