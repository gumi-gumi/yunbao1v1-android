package com.yunbao.one.bean;

/**
 * 0/8.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class LiveTimeChargeBean {
    private int mCoin;
    private boolean mChecked;

    public LiveTimeChargeBean() {
    }

    public LiveTimeChargeBean(int coin) {
        mCoin = coin;
    }

    public LiveTimeChargeBean(int coin, boolean checked) {
        mCoin = coin;
        mChecked = checked;
    }

    public int getCoin() {
        return mCoin;
    }

    public void setCoin(int coin) {
        mCoin = coin;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }
}
