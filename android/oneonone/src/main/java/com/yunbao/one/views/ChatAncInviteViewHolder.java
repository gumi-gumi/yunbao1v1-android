package com.yunbao.one.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.Constants;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.im.utils.ChatLiveImUtil;
import com.yunbao.one.R;
import com.yunbao.one.activity.ChatAnchorActivity;
import com.yunbao.one.activity.ChatBaseActivity;

/**
 * 2019/4/19.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatAncInviteViewHolder extends AbsChatInviteViewHolder implements View.OnClickListener {

    private ImageView mAvatar;
    private TextView mName;
    private TextView mTip;
    private View mBtnCancel;
    private View mBtnAccept;
    private ImageView mImgAccept;

    public ChatAncInviteViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_chat_invite_anc;
    }

    @Override
    public void init() {
        mAvatar = (ImageView) findViewById(R.id.avatar);
        mName = (TextView) findViewById(R.id.name);
        mTip = (TextView) findViewById(R.id.tip);
        mBtnCancel = findViewById(R.id.btn_cancel);
        mBtnAccept = findViewById(R.id.btn_accept);
        mBtnCancel.setOnClickListener(this);
        mBtnAccept.setOnClickListener(this);
        mImgAccept = (ImageView) findViewById(R.id.img_accept);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.btn_cancel) {
            ((ChatAnchorActivity) mContext).onBackPressed();
        } else if (i == R.id.btn_accept) {
            ((ChatAnchorActivity) mContext).accpetChat();
        }
    }

    /**
     * 主播向观众发起赴约
     */
    public void showDataAncToAud(String audienceId, String avatar, String name, String sessionId, int chatType, String price) {
        mImgAccept.setImageResource(chatType == Constants.CHAT_TYPE_VIDEO ? R.mipmap.o_chat_invite_accept_video : R.mipmap.o_chat_invite_accept_voice);
        ImgLoader.display(mContext, avatar, mAvatar);
        mName.setText(name);
        mTip.setText(R.string.chat_invite_tip_1);
        ChatLiveImUtil.chatAncToAudStart(audienceId, sessionId, chatType, price);
        playRingMusic();
        ((ChatBaseActivity) mContext).startWait();
    }

    /**
     * 观众向主播发起的
     */
    public void showDataAndToAnc(String avatar, String name, int chatType) {
        mImgAccept.setImageResource(chatType == Constants.CHAT_TYPE_VIDEO ? R.mipmap.o_chat_invite_accept_video : R.mipmap.o_chat_invite_accept_voice);
        ImgLoader.display(mContext, avatar, mAvatar);
        mName.setText(name);
        mTip.setText(chatType == Constants.CHAT_TYPE_VIDEO ? R.string.chat_invite_tip_2 : R.string.chat_invite_tip_3);
        mBtnAccept.setVisibility(View.VISIBLE);
        playRingMusic();
        ((ChatBaseActivity) mContext).startRing();
    }
}
