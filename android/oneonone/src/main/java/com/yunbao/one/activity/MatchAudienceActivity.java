package com.yunbao.one.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.event.MatchSuccessEvent;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.one.R;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * 2019/5/7.
 * 观众匹配
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MatchAudienceActivity extends AbsActivity implements View.OnClickListener {

    public static void forward(Context context, String price, String priceVip, int type) {
        if(!ClickUtil.canClick()){
            return;
        }
        Intent intent = new Intent(context, MatchAudienceActivity.class);
        intent.putExtra(Constants.MATCH_PRICE, price);
        intent.putExtra(Constants.MATCH_PRICE_VIP, priceVip);
        intent.putExtra(Constants.CHAT_TYPE, type);
        context.startActivity(intent);
    }

    private View mMatchTip;
    private TextView mPrice;
    private TextView mPriceVip;
    private int mType;
    private boolean mStartMatch;
    private Handler mHandler;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_match_audience;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.match));
        Intent intent = getIntent();
        String price = intent.getStringExtra(Constants.MATCH_PRICE);
        String priceVip = intent.getStringExtra(Constants.MATCH_PRICE_VIP);
        mType = intent.getIntExtra(Constants.CHAT_TYPE, Constants.CHAT_TYPE_VOICE);
        String coinName = CommonAppConfig.getInstance().getCoinName();
        mMatchTip = findViewById(R.id.match_tip);
        mPrice = findViewById(R.id.price);
        mPriceVip = findViewById(R.id.price_vip);
        findViewById(R.id.btn_close).setOnClickListener(this);
        EventBus.getDefault().register(this);
        startMatch();
    }


    public void startMatch() {
        OneHttpUtil.matchAudience(mType, new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0) {
                    mStartMatch = true;
                    if (mHandler == null) {
                        mHandler = new Handler();
                    }
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new DialogUitl.Builder(mContext)
                                    .setContent(WordUtil.getString(R.string.match_tip_3))
                                    .setConfrimString(WordUtil.getString(R.string.match_tip_4))
                                    .setClickCallback(new DialogUitl.SimpleCallback2() {
                                        @Override
                                        public void onCancelClick() {
                                            onBackPressed();
                                        }

                                        @Override
                                        public void onConfirmClick(Dialog dialog, String content) {
                                            startMatch();
                                        }
                                    })
                                    .setBackgroundDimEnabled(true)
                                    .build().show();
                        }
                    }, 60000);
                } else {
                    ToastUtil.show(msg);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_close) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if (mStartMatch) {
            OneHttpUtil.matchAudienceCancel();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        mHandler = null;
        EventBus.getDefault().unregister(this);
        OneHttpUtil.cancel(OneHttpConsts.MATCH_AUDIENCE);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMatchSuccessEvent(MatchSuccessEvent e) {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        mHandler = null;
        finish();
    }
}
