package com.yunbao.one.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.one.R;
import com.yunbao.one.activity.ChatBaseActivity;

/**
 * 2019/4/21.
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public abstract class AbsChatEndViewHolder extends AbsViewHolder implements View.OnClickListener {

    public static String sChatSessionId;

    public AbsChatEndViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    public AbsChatEndViewHolder(Context context, ViewGroup parentView, Object... args) {
        super(context, parentView, args);
    }

    @Override
    public void init() {
        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    protected abstract void confirmClick();

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_confirm) {
            confirmClick();
        }
    }
}
