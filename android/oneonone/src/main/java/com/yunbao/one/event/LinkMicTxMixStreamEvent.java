package com.yunbao.one.event;

/**
 * 2019/3/25.
 * 腾讯连麦的时候 主播混流
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class LinkMicTxMixStreamEvent {

    private int mType;
    private String mToStream;

    public LinkMicTxMixStreamEvent(int type, String toStream) {
        mType = type;
        mToStream = toStream;
    }

    public int getType() {
        return mType;
    }

    public String getToStream() {
        return mToStream;
    }
}
