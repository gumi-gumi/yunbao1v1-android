package com.yunbao.one.http;

/**
 * 2019/3/21.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class OneHttpConsts {
    public static final String TIME_CHARGE = "timeCharge";
    public static final String GET_ALL_IMPRESS = "getAllImpress";
    public static final String SET_IMPRESS = "setImpress";
    public static final String SET_REPORT = "setReport";
    public static final String GET_LIVE_REPORT_LIST = "getLiveReportList";
    public static final String GET_LINK_MIC_STREAM = "getLinkMicStream";
    public static final String LINK_MIC_SHOW_VIDEO = "linkMicShowVideo";
    public static final String SET_LINK_MIC_ENABLE = "setLinkMicEnable";
    public static final String CHECK_LINK_MIC_ENABLE = "checkLinkMicEnable";
    public static final String GET_LIVE_SDK = "getLiveSdk";
    public static final String GET_TX_LINK_MIC_ACC_URL = "getTxLinkMicAccUrl";
    public static final String GET_IMPRESS_LIST = "getImpressList";
    public static final String SET_AUTH = "setAuth";
    public static final String GET_AUTH = "getAuth";
    public static final String CHAT_ANC_TO_AUD_START = "chatAncToAudStart";
    public static final String CHAT_ANC_TO_AUD_START_2 = "chatAncToAudStart2";
    public static final String CHAT_AUDIENCE_HANG_UP = "chatAudienceHangUp";
    public static final String CHAT_ANCHOR_HANG_UP = "chatAnchorHangUp";
    public static final String CHAT_ANCHOR_ACCPET = "chatAnchorAccpet";
    public static final String CHAT_AUDIENCE_ACCPET = "chatAudienceAccpet";
    public static final String GET_CHAT_EVA_LIST = "getChatEvaList";
    public static final String SET_CHAT_EVA_LIST = "setChatEvaList";
    public static final String GET_USER_EVA_LIST = "getUserEvaList";
    public static final String GET_USER_EVA_CALC = "getUserEvaCalc";
    public static final String AUD_SUBSCRIBE_ANC = "audSubscribeAnc";
    public static final String GET_MY_SUBCRIBE_LIST = "getMySubcribeList";
    public static final String GET_SUBCRIBE_ME_LIST = "getSubcribeMeList";
    public static final String GET_SUBSCRIBE_NUMS = "getSubscribeNums";
    public static final String MATCH_CHECK = "matchCheck";
    public static final String GET_MATCH_INFO = "getMatchInfo";
    public static final String MATCH_ANCHOR = "matchAnchor";
    public static final String MATCH_ANCHOR_CANCEL = "matchAnchorCancel";
    public static final String MATCH_AUDIENCE = "matchAudience";
    public static final String MATCH_AUDIENCE_CANCEL = "matchAudienceCancel";
    public static final String SET_AUTHOR_AUTH = "setAuthorAuth";


}
