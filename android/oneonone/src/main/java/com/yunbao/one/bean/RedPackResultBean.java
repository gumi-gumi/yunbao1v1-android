package com.yunbao.one.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 1/21.
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RedPackResultBean {

    private String avatar;
    private String time;
    private String uid;
    private String userNiceName;
    private String winCoin;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @JSONField(name = "user_nicename")
    public String getUserNiceName() {
        return userNiceName;
    }

    @JSONField(name = "user_nicename")
    public void setUserNiceName(String userNiceName) {
        this.userNiceName = userNiceName;
    }

    @JSONField(name = "win")
    public String getWinCoin() {
        return winCoin;
    }

    @JSONField(name = "win")
    public void setWinCoin(String winCoin) {
        this.winCoin = winCoin;
    }
}
