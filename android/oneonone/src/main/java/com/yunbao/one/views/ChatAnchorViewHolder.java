package com.yunbao.one.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.yunbao.common.Constants;
import com.yunbao.one.R;

/**
 * 0/9.
 * 主播直播间逻辑
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ChatAnchorViewHolder extends AbsChatLiveViewHolder {

    public ChatAnchorViewHolder(Context context, ViewGroup parentView, int chatType) {
        super(context, parentView, chatType);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_chat_anchor;
    }

    @Override
    public void init() {
        super.init();
        if (mChatType == Constants.CHAT_TYPE_VOICE) {
            findViewById(R.id.space).setVisibility(View.VISIBLE);
        }
    }

}
