package com.yunbao.one.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.one.R;
import com.yunbao.one.interfaces.LivePushListener;

/**
 * 2/22.
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public abstract class AbsChatLivePushViewHolder extends AbsViewHolder {

    private View mCameraCover;//摄像头覆盖物，用来关闭摄像头
    protected LivePushListener mLivePushListener;
    protected boolean mCameraFront;//是否是前置摄像头
    protected boolean mFlashOpen;//闪光灯是否开启了
    protected boolean mPaused;

    public AbsChatLivePushViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    public AbsChatLivePushViewHolder(Context context, ViewGroup parentView, Object... args) {
        super(context, parentView, args);
    }

    @Override
    public void init() {
        mCameraFront = true;
        mCameraCover = findViewById(R.id.camera_cover);
    }


    /**
     * 设置推流监听
     */
    public void setLivePushListener(LivePushListener livePushListener) {
        mLivePushListener = livePushListener;
    }


    /**
     * 开始推流
     */
    public abstract void startPush(String pushUrl, boolean pureVoice);

    /**
     * 停止推流
     */
    public abstract void stopPush();

    /**
     * 切换闪光灯
     */
    public abstract void toggleFlash();

    /**
     * 切换摄像头
     */
    public abstract void toggleCamera();

    /**
     * 关麦
     */
    public abstract void setMute(boolean mute);


    public abstract void setBig(boolean big);


    @Override
    public void release() {
        super.release();
        mLivePushListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        release();
    }

    public void showCameraCover() {
        if (mCameraCover != null && mCameraCover.getVisibility() != View.VISIBLE) {
            mCameraCover.setVisibility(View.VISIBLE);
        }
    }

    public void hideCameraCover() {
        if (mCameraCover != null && mCameraCover.getVisibility() == View.VISIBLE) {
            mCameraCover.setVisibility(View.INVISIBLE);
        }
    }

}
