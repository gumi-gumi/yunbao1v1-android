package com.yunbao.one.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.one.R;
import com.yunbao.one.activity.ChatBaseActivity;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public abstract class AbsChatLiveViewHolder extends AbsViewHolder implements View.OnClickListener {

    private boolean mMute;
    private ImageView mMuteIcon;
    private Drawable mMuteDrawable;
    private Drawable mUnMuteDrawable;
    protected int mChatType;

    public AbsChatLiveViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    public AbsChatLiveViewHolder(Context context, ViewGroup parentView, Object... args) {
        super(context, parentView, args);
    }


    @Override
    protected void processArguments(Object... args) {
        mChatType = (int) args[0];
    }

    @Override
    public void init() {
        findViewById(R.id.btn_mute).setOnClickListener(this);
        findViewById(R.id.btn_hang_up).setOnClickListener(this);
        findViewById(R.id.btn_camera_switch).setOnClickListener(this);
        mMuteIcon = (ImageView) findViewById(R.id.mute_icon);
        mMuteDrawable = ContextCompat.getDrawable(mContext, R.mipmap.o_chat_mute_1);
        mUnMuteDrawable = ContextCompat.getDrawable(mContext, R.mipmap.o_chat_mute_0);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_mute) {
            toggleMute();
        } else if (i == R.id.btn_hang_up) {
            ((ChatBaseActivity) mContext).hangUpChat();
        } else if (i == R.id.btn_camera_switch) {
            ((ChatBaseActivity) mContext).switchCamera();
        }
    }


    private void toggleMute() {
        mMute = !mMute;
        ((ChatBaseActivity) mContext).setMute(mMute);
        if (mMuteIcon != null) {
            mMuteIcon.setImageDrawable(mMute ? mMuteDrawable : mUnMuteDrawable);
        }
    }

    public void show() {
        if (mContentView != null) {
            mContentView.setVisibility(View.VISIBLE);
        }
    }

    public void hide() {
        if (mContentView != null) {
            mContentView.setVisibility(View.GONE);
        }
    }

}
