package com.yunbao.one.views;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.ViewGroup;

import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.one.R;

/**
 * 2019/4/20.
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public abstract class AbsChatInviteViewHolder extends AbsViewHolder {

    private MediaPlayer mMediaPlayer;

    public AbsChatInviteViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    /**
     * 播放铃声
     */
    public void playRingMusic() {
        try {
            mMediaPlayer = MediaPlayer.create(mContext, R.raw.ring);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setVolume(1f, 1f);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 停止播放铃声
     */
    private void stopRingMusic() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public void onDestroy() {
        release();
        super.onDestroy();
    }

    public void hide() {
        release();
        removeFromParent();
    }

    public void release(){
        stopRingMusic();
    }
}
