package com.yunbao.main.views;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.custom.ZoomView;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.FileUtil;
import com.yunbao.common.utils.PermissionUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.main.R;
import com.yunbao.main.activity.EditProfileActivity;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class PhotoDetailViewHolder extends AbsViewHolder implements View.OnClickListener {

    private ZoomView mImg;
    private String mPhotoUrl;
    private boolean mShowMore;
    private ActionLister mActionLister;
    private boolean mDestory;
    private Bitmap bitmap;
    private Dialog mChooseImageDialog;

    public PhotoDetailViewHolder(Context context, ViewGroup parentView, String photoUrl, boolean showMore) {
        super(context, parentView, photoUrl, showMore);
    }

    @Override
    protected void processArguments(Object... args) {
        mPhotoUrl = (String) args[0];
        mShowMore = (boolean) args[1];
        //图片转成Bitmap数组
        new Thread(new Runnable(){
            @Override
            public void run() {
                bitmap = FileUtil.getBitmap(mPhotoUrl);
            }
        }).start();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_photo_detail;
    }

    @Override
    public void init() {
        setStatusHeight();
        mImg = findViewById(R.id.img);
        View btnMore = findViewById(R.id.btn_more);
        if (mShowMore) {
            btnMore.setOnClickListener(this);
        } else {
            btnMore.setVisibility(View.INVISIBLE);
        }
        ZoomView.SingleTapConfirmedListener listener = new ZoomView.SingleTapConfirmedListener() {
            @Override
            public void onSingleTapConfirmed() {
                ((AbsActivity) mContext).finish();
            }
        };
        ImgLoader.display(mContext, mPhotoUrl, mImg);
        mImg.setOnSingleTapConfirmedListener(listener);
        mImg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //长按后显示的 Item
                PermissionUtil.request((AbsActivity)mContext,
                        new CommonCallback<Boolean>() {
                            @Override
                            public void callback(Boolean result) {
                                if (result){
                                    openWindow(true);
                                }else{
                                    openWindow(false);
                                }
                            }

                        },
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                );


                return true;
    }
});

    }

    private void openWindow(final boolean permission){
        mChooseImageDialog = DialogUitl.getStringArrayDialog(mContext, new Integer[]{
                R.string.save}, true, new DialogUitl.StringArrayDialogCallback() {
            @Override
            public void onItemClick(String text, int tag) {
                if (tag == R.string.save) {
                    if (permission) {
                        if (bitmap != null) {
                            FileUtil.saveImageToGallery(mContext, bitmap);
                        } else {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    /**
                                     *要执行的操作
                                     */
                                    FileUtil.saveImageToGallery(mContext, bitmap);
                                }
                            }, 2000);//2秒后执行Runnable中的run方法
                        }
                    }else{
                        ToastUtil.show(R.string.save_fail);
                    }

                }
            }

        });
        mChooseImageDialog.show();
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_more) {
            if (mActionLister != null) {
                mActionLister.onMoreClick();
            }
        }
    }


    public void setActionLister(ActionLister actionLister) {
        mActionLister = actionLister;
    }

    public interface ActionLister {
        void onMoreClick();
    }


    @Override
    public void onDestroy() {
        mDestory = true;
        mActionLister = null;
        super.onDestroy();
    }

}
