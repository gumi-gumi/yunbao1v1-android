package com.yunbao.main.views;

import android.content.Context;
import android.view.ViewGroup;

import com.yunbao.main.activity.MainActivity;
import com.yunbao.one.bean.ChatLiveBean;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public abstract class AbsMainHomeChildViewHolder extends AbsMainViewHolder {


    public AbsMainHomeChildViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    /**
     * 观看直播
     */
    public void forwardUserHome(String toUid) {
        ((MainActivity) mContext).forwardUserHome(toUid);
    }
}
