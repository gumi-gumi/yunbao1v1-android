package com.yunbao.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.bean.SubcribeAncBean;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class SubcribeAncAdapter extends RefreshAdapter<SubcribeAncBean> {

    //    private View.OnClickListener mClickListener;
    private View.OnClickListener mToClickListener;
    private ActionListener mActionListener;
    private String mIdString;

    public SubcribeAncAdapter(Context context) {
        super(context);
//        mClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!canClick()) {
//                    return;
//                }
//                Object tag = v.getTag();
//                if (tag == null) {
//                    return;
//                }
//                SubcribeAncBean bean = (SubcribeAncBean) tag;
//                if (mActionListener != null) {
//                    mActionListener.onItemClick(bean);
//                }
//            }
//        };

        mToClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!canClick()) {
                    return;
                }
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                SubcribeAncBean bean = (SubcribeAncBean) tag;
                if (mActionListener != null) {
                    mActionListener.onToSubcribeClick(bean);
                }
            }
        };
        mIdString = WordUtil.getString(R.string.search_id);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(mInflater.inflate(R.layout.item_subcribe_anc, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {
        ((Vh) vh).setData(mList.get(position));
    }

    class Vh extends RecyclerView.ViewHolder {

        ImageView mAvatar;
        TextView mName;
        TextView mId;
        View mBtnToSubcribe;
        ImageView mVideoOrVoice;

        public Vh(View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mId = itemView.findViewById(R.id.id_val);
            mVideoOrVoice = itemView.findViewById(R.id.video_or_voice);
            mBtnToSubcribe = itemView.findViewById(R.id.btn_subcribe_to);
            mBtnToSubcribe.setOnClickListener(mToClickListener);
            //itemView.setOnClickListener(mClickListener);
        }

        void setData(SubcribeAncBean bean) {
            itemView.setTag(bean);
            mBtnToSubcribe.setTag(bean);
            ImgLoader.displayAvatar(mContext, bean.getAvatar(), mAvatar);
            mName.setText(bean.getUserNiceName());
            mId.setText(StringUtil.contact(mIdString, bean.getUid()));
            if (bean.getType() == 1) {
                mVideoOrVoice.setImageResource(R.mipmap.o_main_use_video);
            } else {
                mVideoOrVoice.setImageResource(R.mipmap.o_main_use_voice);
            }
        }
    }

    public interface ActionListener {
        void onItemClick(SubcribeAncBean u);

        void onToSubcribeClick(SubcribeAncBean u);
    }

    public void setActionListener(ActionListener actionListener) {
        mActionListener = actionListener;
    }
}
