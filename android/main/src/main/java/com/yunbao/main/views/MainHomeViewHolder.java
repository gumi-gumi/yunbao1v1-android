package com.yunbao.main.views;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.SearchActivity;

/**
 * MainActivity 首页
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainHomeViewHolder extends AbsMainHomeParentViewHolder implements View.OnClickListener {

    private MainHomeRecommendViewHolder mRecommendViewHolder;
    private MainHomeUserViewHolder mUserViewHolder;
    private View mBtnSearch;


    public MainHomeViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_home;
    }

    @Override
    public void init() {
        setStatusHeight();
        super.init();
        mBtnSearch = findViewById(R.id.btn_search);
        mBtnSearch.setOnClickListener(this);
    }


    @Override
    protected void loadPageData(int position) {
        if (mViewHolders == null) {
            return;
        }
        AbsMainHomeChildViewHolder vh = mViewHolders[position];
        if (vh == null) {
            if (mViewList != null && position < mViewList.size()) {
                FrameLayout parent = mViewList.get(position);
                if (parent == null) {
                    return;
                }
                if (position == 0) {
                    mRecommendViewHolder = new MainHomeRecommendViewHolder(mContext, parent);
                    vh = mRecommendViewHolder;
                } else if (position == 1) {
                    mUserViewHolder = new MainHomeUserViewHolder(mContext, parent);
                    vh = mUserViewHolder;
                }
                if (vh == null) {
                    return;
                }
                mViewHolders[position] = vh;
                vh.addToParent();
                vh.subscribeActivityLifeCycle();
            }
        }
        if (vh != null) {
            vh.loadData();
        }

    }

    @Override
    protected int getPageCount() {
        return 2;
    }

    @Override
    protected String[] getTitles() {
        return new String[]{
                WordUtil.getString(R.string.recommend),
                WordUtil.getString(R.string.home_user)
        };
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_search) {
            SearchActivity.forward(mContext);
        }
    }


}
