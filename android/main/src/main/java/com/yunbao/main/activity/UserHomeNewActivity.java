package com.yunbao.main.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.bean.ChatReceiveGiftBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.event.FollowEvent;
import com.yunbao.common.http.CommonHttpUtil;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.presenter.GiftAnimViewHolder;
import com.yunbao.common.utils.ProcessResultUtil;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.im.bean.ImMessageBean;
import com.yunbao.im.dialog.ChatGiftDialogFragment;
import com.yunbao.im.presenter.CheckChatPresenter;
import com.yunbao.main.R;
import com.yunbao.main.views.UserHomeViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
@Route(path = RouteUtil.PATH_USER_HOME)
public class UserHomeNewActivity extends AbsActivity implements ChatGiftDialogFragment.ActionListener {
    private ViewGroup mRoot;
    private String mToUid;
    private JSONObject mUserObj;
    private UserBean mUserBean;
    private CheckChatPresenter mCheckChatPresenter;
    private ProcessResultUtil mProcessResultUtil;
    private GiftAnimViewHolder mGiftAnimViewHolder;
    private boolean mPaused;
    private UserHomeViewHolder mUserHomeViewHolder;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_home_new;
    }

    @Override
    protected void main() {
        super.main();
        mRoot = findViewById(R.id.root);
        Intent intent = getIntent();
        mToUid = intent.getStringExtra(Constants.TO_UID);
        if (TextUtils.isEmpty(mToUid)) {
            return;
        }
        mProcessResultUtil = new ProcessResultUtil(this);
        EventBus.getDefault().register(this);

        String data = intent.getStringExtra(Constants.USER_BEAN);
        if (!TextUtils.isEmpty(data)) {
            setUserObj(JSON.parseObject(data));
        }
    }

    private void setUserObj(JSONObject obj) {
        if (obj == null) {
            return;
        }
        mUserObj = obj;
        mUserBean =JSON.toJavaObject(obj, UserBean.class);
        mUserHomeViewHolder = new UserHomeViewHolder(mContext, mRoot, mToUid, mUserBean);
        mUserHomeViewHolder.addToParent();
        mUserHomeViewHolder.subscribeActivityLifeCycle();
        mUserHomeViewHolder.loadData();
    }

    public JSONObject getUserObj() {
        return mUserObj;
    }

    public UserBean getUserBean() {
        return mUserBean;
    }

    public void userHomeClick(View v) {
        if(!canClick()){
            return;
        }
        int i = v.getId();
         if (i == R.id.btn_follow) {
            followClick();
        } else if (i == R.id.btn_gift) {
            giftClick();
        } else if (i == R.id.btn_chat) {
            checkPermissions();
        }
    }

    /**
     * 检查权限
     */
    private void checkPermissions() {
        mProcessResultUtil.requestPermissions(new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
        }, new CommonCallback<Boolean>() {
            @Override
            public void callback(Boolean result) {
                if (result) {
                    chatClick();
                }
            }
        });
    }

    /**
     * 点击通话
     */
    private void chatClick() {
        if (mUserObj == null) {
            return;
        }
        boolean openVideo = mUserObj.getIntValue("isvideo") == 1;
         if (openVideo) {
            chatAudToAncStart(Constants.CHAT_TYPE_VIDEO);
        }else {
            ToastUtil.show(R.string.user_home_close_all);
        }
    }

    /**
     * 点击礼物
     */
    private void giftClick() {
        if (mUserBean == null) {
            return;
        }
        ChatGiftDialogFragment fragment = new ChatGiftDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LIVE_UID, mUserBean.getId());
        bundle.putString(Constants.CHAT_SESSION_ID, "0");
        fragment.setArguments(bundle);
        fragment.setActionListener(this);
        fragment.show(getSupportFragmentManager(), "ChatGiftDialogFragment");
    }

    /**
     * 点击关注
     */
    private void followClick() {
        CommonHttpUtil.setAttention(mToUid, null);
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFollowEvent(FollowEvent e) {
        if (mUserBean != null) {
            mUserBean.setAttent(e.getIsAttention());
        }
    }


    /**
     * 观众向主播发起通话邀请
     *
     * @param type 通话类型
     */
    private void chatAudToAncStart(int type) {
        if (mUserBean == null) {
            return;
        }
        if (mCheckChatPresenter == null) {
            mCheckChatPresenter = new CheckChatPresenter(mContext);
        }
        mCheckChatPresenter.chatAudToAncStart(mToUid, type, mUserBean);
    }

    /**
     * 收到消息的回调
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onImMessageBean(ImMessageBean bean) {
        if (mPaused) {
            return;
        }
        if (!TextUtils.isEmpty(mToUid) && mToUid.equals(bean.getUid())) {
            ChatReceiveGiftBean giftBean = bean.getGiftBean();
            if (giftBean != null) {
                showGift(bean.getGiftBean());
            }
        }
    }

    /**
     * 显示礼物动画
     */
    public void showGift(ChatReceiveGiftBean bean) {
        if (mGiftAnimViewHolder == null) {
            mGiftAnimViewHolder = new GiftAnimViewHolder(mContext, mRoot);
            mGiftAnimViewHolder.addToParent();
        }
        mGiftAnimViewHolder.showGiftAnim(bean);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPaused = false;
    }

    @Override
    public void onDestroy() {
        if (mUserHomeViewHolder != null) {
            mUserHomeViewHolder.removeFromParent();
            mUserHomeViewHolder.release();
        }
        EventBus.getDefault().unregister(this);
        if (mCheckChatPresenter != null) {
            mCheckChatPresenter.cancel();
        }
        mCheckChatPresenter = null;
        if (mProcessResultUtil != null) {
            mProcessResultUtil.release();
        }
        mProcessResultUtil = null;
        if (mGiftAnimViewHolder != null) {
            mGiftAnimViewHolder.release();
        }
        mGiftAnimViewHolder = null;
        super.onDestroy();
    }

    @Override
    public void onChargeClick() {
        RouteUtil.forwardMyCoin();
    }



}
