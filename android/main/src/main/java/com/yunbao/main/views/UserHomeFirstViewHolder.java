package com.yunbao.main.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.CommonIconUtil;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.UserHomeNewActivity;
import com.yunbao.main.bean.WallBean;
import com.yunbao.main.dialog.ShowHomeDialogFragment;
import com.yunbao.one.views.AbsUserHomeViewHolder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class UserHomeFirstViewHolder extends AbsUserHomeViewHolder {

    private List<UserHomeWallViewHolder> mViewList;
    private ViewPager mViewPager;
    private RadioGroup mRadioGroup;
    private TextView mName;
    private TextView mCity;
    private TextView mID;
    private TextView mFans;
    private TextView mVideoPrice;
    private TextView mVoicePrice;
    private ImageView mSex;
    private ImageView mLevelAnchor;
    private View mVip;
    private ImageView mOnLine;
    private ImageView mBtnFollow;
    private Drawable mUnFollowDrawable;
    private Drawable mFollowDrawable;
    private ImageView mBtnChat;
    private String mToUid;
    public int mCurrentPos;

    public UserHomeFirstViewHolder(Context context, ViewGroup parentView, String toUid) {
        super(context, parentView, toUid);
    }

    @Override
    protected void processArguments(Object... args) {
        mToUid = (String) args[0];
    }


    @Override
    protected int getLayoutId() {
        return R.layout.view_user_home_first;
    }

    @Override
    public void init() {
        setStatusHeight();
        mUnFollowDrawable = ContextCompat.getDrawable(mContext, R.mipmap.o_user_btn_follow_1_0);
        mFollowDrawable = ContextCompat.getDrawable(mContext, R.mipmap.o_user_btn_follow_1_1);
        mBtnFollow = (ImageView) findViewById(R.id.btn_follow);
        mName = (TextView) findViewById(R.id.name);
        mCity = (TextView) findViewById(R.id.city);
        mID = (TextView) findViewById(R.id.id_val);
        mFans = (TextView) findViewById(R.id.fans);
        mVideoPrice = (TextView) findViewById(R.id.price_video);
        mVoicePrice = (TextView) findViewById(R.id.price_voice);
        mSex = (ImageView) findViewById(R.id.sex);
        mLevelAnchor = (ImageView) findViewById(R.id.level_anchor);
        mVip = findViewById(R.id.vip);
        mOnLine = (ImageView) findViewById(R.id.on_line);
        mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);
        mBtnChat = (ImageView) findViewById(R.id.btn_chat);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mRadioGroup != null) {
                    ((RadioButton) mRadioGroup.getChildAt(position)).setChecked(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void loadData() {
        JSONObject obj = ((UserHomeNewActivity) mContext).getUserObj();
        if (obj == null) {
            return;
        }
        if (!isFirstLoadData()) {
            return;
        }
        if (mName != null) {
            mName.setText(obj.getString("user_nickname"));
        }
        if (mCity != null) {
            mCity.setText(obj.getString("city"));
        }
        if (mID != null) {
            mID.setText(StringUtil.contact("ID:", obj.getString("id")));
        }
        if (mFans != null) {
            mFans.setText(String.format(WordUtil.getString(R.string.user_home_fans), obj.getString("fans")));
        }
        CommonAppConfig appConfig = CommonAppConfig.getInstance();
        String coinName = appConfig.getCoinName();
        boolean openVideo = obj.getIntValue("isvideo") == 1;
        boolean openVoice = obj.getIntValue("isvoice") == 1;
        if (mBtnChat != null) {
            if (!openVideo && openVoice) {
                mBtnChat.setImageResource(R.mipmap.o_user_btn_chat_voice);
            } else if (!openVoice && openVideo) {
                mBtnChat.setImageResource(R.mipmap.o_user_btn_chat_video);
            }
        }
        if (mVideoPrice != null) {
            if (openVideo) {
                mVideoPrice.setText(String.format(WordUtil.getString(R.string.user_home_video_price)
                        , StringUtil.contact(obj.getString("video_value"), coinName)));
            } else {
                mVideoPrice.setText(R.string.user_home_price_close_video);
            }
        }

        if (mSex != null) {
            mSex.setImageResource(CommonIconUtil.getSexIcon(obj.getIntValue("sex")));
        }

        if (mVip != null) {
            if (obj.getIntValue("isvip") == 1) {
                if (mVip.getVisibility() != View.VISIBLE) {
                    mVip.setVisibility(View.VISIBLE);
                }
            } else {
                if (mVip.getVisibility() != View.GONE) {
                    mVip.setVisibility(View.GONE);
                }
            }
        }
        if (mOnLine != null) {
            mOnLine.setImageResource(CommonIconUtil.getOnLineIcon2(obj.getIntValue("online")));
        }
        List<WallBean> photos = JSON.parseArray(obj.getString("photos_list"), WallBean.class);
        setImageList(photos);
        setFollow(obj.getIntValue("isattent") == 1);
    }

    private void setImageList(final List<WallBean> imageList) {
        if (imageList == null || mRadioGroup == null || mViewPager == null) {
            return;
        }
        final int size = imageList.size();
        if (size == 0) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(mContext);
        for (int i = 0; i < size; i++) {
            RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.view_user_indicator, mRadioGroup, false);
            radioButton.setId(i + 10000);
            if (i == 0) {
                radioButton.setChecked(true);
            }
            mRadioGroup.addView(radioButton);
        }
        mViewList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            UserHomeWallViewHolder vh = new UserHomeWallViewHolder(mContext, mViewPager);
            mViewList.add(vh);
        }
        if (size > 1) {
            mViewPager.setOffscreenPageLimit(size - 1);
        }
        mViewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return size;
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {
                UserHomeWallViewHolder vh = mViewList.get(position);
                vh.addToParent();
                vh.loadData(imageList.get(position));
                return vh.getContentView();
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            }
        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPos = position;
                setVideoPause(mCurrentPos != 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setFollow(boolean follow) {
        if (mBtnFollow != null) {
            mBtnFollow.setImageDrawable(follow ? mFollowDrawable : mUnFollowDrawable);
        }
    }

    public void setVideoPause(boolean pause) {
        if (mViewList != null && mViewList.size() > 0) {
            UserHomeWallViewHolder vh = mViewList.get(0);
            if (vh != null) {
                if (pause) {
                    vh.passivePause();
                } else {
                    vh.passiveResume();
                }
            }
        }
    }

    public void setVideoMute(boolean mute) {
        if (mViewList != null && mViewList.size() > 0) {
            UserHomeWallViewHolder vh = mViewList.get(0);
            if (vh != null) {
              vh.passiveMute(mute);

            }
        }
    }
}
