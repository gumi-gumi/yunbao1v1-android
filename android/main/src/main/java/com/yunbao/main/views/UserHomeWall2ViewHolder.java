package com.yunbao.main.views;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.custom.ZoomView;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.FileUtil;
import com.yunbao.common.utils.PermissionUtil;
import com.yunbao.common.utils.ScreenDimenUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.main.R;
import com.yunbao.main.activity.UserHomeNewActivity;
import com.yunbao.main.bean.WallBean;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class UserHomeWall2ViewHolder extends AbsViewHolder implements UserHomeWallVideoViewHolder.ActionListener {


    private ZoomView mCover;
    private ImageView mCover1;
    private boolean mFristLoad;
    private UserHomeWallVideoViewHolder mVideoVh;
    private int mCurrentPos;
    private WallBean mWallBean;
    private Dialog mChooseImageDialog;
    private Bitmap bitmap;

    public UserHomeWall2ViewHolder(Context context, ViewGroup parentView, int pos) {
        super(context, parentView, pos);
    }


    @Override
    protected void processArguments(Object... args) {
        mCurrentPos = (int) args[0];
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_user_home_wall_2;
    }

    @Override
    public void init() {
        mFristLoad = true;
        mCover =findViewById(R.id.img);
    }


    public void loadData(final WallBean wallBean) {
        if (!mFristLoad || wallBean == null) {
            return;
        }
        mFristLoad = false;
        mWallBean = wallBean;
        if (wallBean.isVideo()) {
            findViewById(R.id.root).setBackgroundColor(0xff000000);
//            mCover1.setVisibility(View.VISIBLE);
//            mCover.setVisibility(View.GONE);
//            ImgLoader.displayDrawable(mContext, wallBean.getThumb(), new ImgLoader.DrawableCallback() {
//                @Override
//                public void onLoadSuccess(Drawable drawable) {
//                    if (mCover1 != null && mCover1.getVisibility() == View.VISIBLE && drawable != null) {
//                        float w = drawable.getIntrinsicWidth();
//                        float h = drawable.getIntrinsicHeight();
//                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mCover1.getLayoutParams();
//                        int targetH = 0;
//                        if (w / h > 0.5625f) {//横屏  9:16=0.5625
//                            targetH = (int) (ScreenDimenUtil.getInstance().getScreenWdith() / w * h);
//                        } else {
//                            targetH = ViewGroup.LayoutParams.MATCH_PARENT;
//                        }
//                        if (targetH != params.height) {
//                            params.height = targetH;
//                            params.gravity = Gravity.CENTER;
//                            mCover1.requestLayout();
//                        }
//                        mCover1.setImageDrawable(drawable);
//                    }
//                }
//
//                @Override
//                public void onLoadFailed() {
//
//                }
//            });
            ImgLoader.display(mContext, wallBean.getThumb(), mCover);
            UserHomeWallVideoViewHolder vh = new UserHomeWallVideoViewHolder(mContext, (ViewGroup) findViewById(R.id.video_container));
            mVideoVh = vh;
            vh.addToParent();
            vh.subscribeActivityLifeCycle();
            vh.setActionListener(this);

            if (mCurrentPos == 0) {
                vh.startPlay(wallBean.getHref());
//                ((UserHomeNewActivity)mContext).voiceStop();
            }
        } else {
            ImgLoader.display(mContext, wallBean.getThumb(), mCover);
            //图片转成Bitmap数组
            new Thread(new Runnable(){
                @Override
                public void run() {
                    bitmap = FileUtil.getBitmap(wallBean.getThumb());
                }
            }).start();
            mCover.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //长按后显示的 Item
                    PermissionUtil.request((AbsActivity)mContext,
                            new CommonCallback<Boolean>() {
                                @Override
                                public void callback(Boolean result) {
                                    if (result){
                                        openWindow(true);
                                    }else{
                                        openWindow(false);
                                    }
                                }

                            },
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    );


                    return true;
                }
            });
        }

    }
    private void openWindow(final boolean permission){
        mChooseImageDialog = DialogUitl.getStringArrayDialog(mContext, new Integer[]{
                R.string.save}, true, new DialogUitl.StringArrayDialogCallback() {
            @Override
            public void onItemClick(String text, int tag) {
                if (tag == R.string.save) {
                    if (permission) {
                        if (bitmap != null) {
                            FileUtil.saveImageToGallery(mContext, bitmap);
                        } else {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    /**
                                     *要执行的操作
                                     */
                                    FileUtil.saveImageToGallery(mContext, bitmap);
                                }
                            }, 2000);//2秒后执行Runnable中的run方法
                        }
                    }else{
                        ToastUtil.show(R.string.save_fail);
                    }

                }
            }

        });
        mChooseImageDialog.show();
    }

    /**
     * 被动暂停播放
     */
    public void passivePause() {
        if (mVideoVh != null) {
//            mVideoVh.passivePause();
            mVideoVh.stopPlay();
        }
    }

    public void startPlay() {
        if (mVideoVh != null && mWallBean != null) {
            mVideoVh.startPlay(mWallBean.getHref());
        }
    }

    /**
     * 被动恢复播放
     */
    public void passiveResume() {
        if (mVideoVh != null) {
//            mVideoVh.passiveResume();
            mVideoVh.startPlay(mWallBean.getHref());
        }
    }


    @Override
    public void onFirstFrame() {
        if (mCover != null && mCover.getVisibility() == View.VISIBLE) {
            mCover.setVisibility(View.INVISIBLE);
        }
    }
}
