package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.main.R;
import com.yunbao.main.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MyAuthActivity extends AbsActivity {

    @BindView(R2.id.titleView)
    TextView titleView;
    @BindView(R2.id.btn_back)
    ImageView btnBack;
    TextView name;
    TextView cd;
    TextView phone;

    private String realName,cardNo,phoneNumber;


    public static void forward(Context context,String name,String cardNo,String phone) {
        Intent intent = new Intent(context, MyAuthActivity.class);
        intent.putExtra("name",name);
        intent.putExtra("cardNo",cardNo);
        intent.putExtra("phone",phone);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_auth;
    }

    @Override
    protected void main() {
        name=findViewById(R.id.name);
        cd=findViewById(R.id.cd);
        phone=findViewById(R.id.phone);
        Intent intent=getIntent();
        realName=intent.getStringExtra("name");
        cardNo=intent.getStringExtra("cardNo");
        phoneNumber=intent.getStringExtra("phone");

        name.setText(realName);
        cd.setText(cardNo);
        phone.setText(phoneNumber);

    }


}
