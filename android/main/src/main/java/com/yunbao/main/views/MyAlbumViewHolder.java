package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.custom.ItemDecoration;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.main.R;
import com.yunbao.main.activity.PhotoDetailActivity;
import com.yunbao.main.adapter.MyPhotoAdapter;
import com.yunbao.main.bean.PhotoBean;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;
import com.yunbao.one.views.AbsUserHomeViewHolder;

import java.util.Arrays;
import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MyAlbumViewHolder extends AbsUserHomeViewHolder implements OnItemClickListener<PhotoBean> {

    private CommonRefreshView mRefreshView;
    private MyPhotoAdapter mAdapter;

    public MyAlbumViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.view_my_album;
    }

    @Override
    public void init() {
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        mRefreshView.setEmptyLayoutId(R.layout.view_no_data_album_home);
        mRefreshView.setLayoutManager(new GridLayoutManager(mContext, 3, GridLayoutManager.VERTICAL, false));
        ItemDecoration decoration = new ItemDecoration(mContext, 0x00000000, 2, 0);
        decoration.setOnlySetItemOffsetsButNoDraw(true);
        mRefreshView.setItemDecoration(decoration);
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<PhotoBean>() {
            @Override
            public RefreshAdapter<PhotoBean> getAdapter() {
                if (mAdapter == null) {
                    mAdapter = new MyPhotoAdapter(mContext);
                    mAdapter.setOnItemClickListener(MyAlbumViewHolder.this);
                }
                return mAdapter;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                MainHttpUtil.getMyAlbum(p,null, callback);
            }

            @Override
            public List<PhotoBean> processData(String[] info) {
                return JSON.parseArray(Arrays.toString(info), PhotoBean.class);
            }

            @Override
            public void onRefreshSuccess(List<PhotoBean> list, int listCount) {
            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<PhotoBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
    }

    @Override
    public void loadData() {
        if (mRefreshView != null) {
            mRefreshView.initData();
        }
    }


    @Override
    public void onItemClick(PhotoBean bean, int position) {
        PhotoDetailActivity.forward(mContext, bean);
    }


    @Override
    public void release() {
        MainHttpUtil.cancel(MainHttpConsts.GET_MY_ALBUM);
    }

    @Override
    public void onDestroy() {
        release();
        super.onDestroy();
    }
}
