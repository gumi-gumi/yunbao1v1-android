package com.yunbao.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.bean.UserItemBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;

import java.util.ArrayList;
import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainMeAdapter extends RecyclerView.Adapter {

    private static final int NORMAL = 0;
    private static final int RADIO = 1;
    private static final int RADIO_TEXT = 2;

    private Context mContext;
    private List<UserItemBean> mList;
    private LayoutInflater mInflater;
    private View.OnClickListener mOnClickListener;
    private View.OnClickListener mOnRadioBtnClickListener;
    private ActionListener mActionListener;
    private Drawable mRadioCheckDrawable;
    private Drawable mRadioUnCheckDrawable;
    private String mPriceSuffix;

    public MainMeAdapter(Context context) {
        mContext = context;
        mList = new ArrayList<>();
        mInflater = LayoutInflater.from(context);
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                int position = (int) tag;
                UserItemBean bean = mList.get(position);
                if (mActionListener != null) {
                    mActionListener.onItemClick(bean);
                }
            }
        };
        mOnRadioBtnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                int position = (int) tag;
                UserItemBean bean = mList.get(position);
                bean.toggleRadioBtn();
                notifyItemChanged(position, Constants.PAYLOAD);
                if (mActionListener != null) {
                    mActionListener.onRadioBtnChanged(bean);
                }
            }
        };
        mRadioCheckDrawable = ContextCompat.getDrawable(context, R.mipmap.icon_btn_radio_1);
        mRadioUnCheckDrawable = ContextCompat.getDrawable(context, R.mipmap.icon_btn_radio_0);
        mPriceSuffix = String.format(WordUtil.getString(R.string.main_price), CommonAppConfig.getInstance().getCoinName());
    }

    public void setActionListener(ActionListener actionListener) {
        mActionListener = actionListener;
    }

    @Override
    public int getItemViewType(int position) {
        UserItemBean bean = mList.get(position);
        int id = bean.getId();
        if (id == Constants.MAIN_ME_DISTURB) {
            return RADIO;
        } else if (id == Constants.MAIN_ME_VIDEO || id == Constants.MAIN_ME_VOICE) {
            return RADIO_TEXT;
        }
        return NORMAL;
    }


    public void setList(List<UserItemBean> list) {
        if (list == null) {
            return;
        }
        boolean changed = false;
        if (mList.size() != list.size()) {
            changed = true;
        } else {
            for (int i = 0, size = mList.size(); i < size; i++) {
                if (!mList.get(i).equals(list.get(i))) {
                    changed = true;
                    break;
                }
            }
        }
        if (changed) {
            mList = list;
            notifyDataSetChanged();
        }
    }

    public void updateVideoPrice(String videoPrice) {
        for (int i = 0, size = mList.size(); i < size; i++) {
            UserItemBean bean = mList.get(i);
            if (bean.getId() == Constants.MAIN_ME_VIDEO) {
                bean.setPriceText(videoPrice);
                notifyItemChanged(i);
                break;
            }
        }
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == RADIO) {
            return new RadioButtonVh(mInflater.inflate(R.layout.item_main_me_1, parent, false));
        } else if (viewType == RADIO_TEXT) {
            return new RadioButtonTextVh(mInflater.inflate(R.layout.item_main_me_2, parent, false));
        }
        return new Vh(mInflater.inflate(R.layout.item_main_me_0, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position, @NonNull List payloads) {
        Object payload = payloads.size() > 0 ? payloads.get(0) : null;
        if (vh instanceof Vh) {
            ((Vh) vh).setData(mList.get(position), position, payload);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }



    class Vh extends RecyclerView.ViewHolder {

        ImageView mThumb;
        TextView mName;

        public Vh(View itemView) {
            super(itemView);
            mThumb = itemView.findViewById(R.id.thumb);
            mName = itemView.findViewById(R.id.name);
            itemView.setOnClickListener(mOnClickListener);
        }

        void setData(UserItemBean bean, int position, Object payload) {
            if (payload == null) {
                itemView.setTag(position);
                ImgLoader.display(mContext, bean.getThumb(), mThumb);
                mName.setText(bean.getName());
            }
        }
    }

    class RadioButtonVh extends Vh {

        ImageView mBtnRadio;

        public RadioButtonVh(View itemView) {
            super(itemView);
            mBtnRadio = itemView.findViewById(R.id.btn_radio);
            mBtnRadio.setOnClickListener(mOnRadioBtnClickListener);
        }

        @Override
        void setData(UserItemBean bean, int position, Object payload) {
            super.setData(bean, position, payload);
            if (payload == null) {
                mBtnRadio.setTag(position);
            }
            mBtnRadio.setImageDrawable(bean.isRadioBtnChecked() ? mRadioCheckDrawable : mRadioUnCheckDrawable);
        }
    }

    class RadioButtonTextVh extends RadioButtonVh {

        TextView mText;

        public RadioButtonTextVh(View itemView) {
            super(itemView);
            mText = itemView.findViewById(R.id.text);
        }

        @Override
        void setData(UserItemBean bean, int position, Object payload) {
            super.setData(bean, position, payload);
            mText.setText(StringUtil.contact(bean.getPriceText(), mPriceSuffix));
        }
    }



    public interface ActionListener {
        void onItemClick(UserItemBean bean);

        void onRadioBtnChanged(UserItemBean bean);
    }


}
