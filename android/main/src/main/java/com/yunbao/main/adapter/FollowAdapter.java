package com.yunbao.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class FollowAdapter extends RefreshAdapter<UserBean> {

    private View.OnClickListener mClickListener;
    private View.OnClickListener mFollowClickListner;
    private ActionListener mActionListener;
    private String mIdString;
    private String mFanString;
    private String mFollowAddString;
    private String mFollowCancelString;
    private Drawable mFollowAddDrawable;
    private Drawable mFollowCancelDrawable;
    private int mFollowAddColor;
    private int mFollowCancelColor;


    public FollowAdapter(Context context) {
        super(context);
        mFollowAddString = WordUtil.getString(R.string.follow_add);
        mFollowCancelString = WordUtil.getString(R.string.follow_cancel);
        mFollowAddDrawable = ContextCompat.getDrawable(context, R.drawable.btn_follow_1);
        mFollowCancelDrawable = ContextCompat.getDrawable(context, R.drawable.btn_follow_0);
        mFollowAddColor = ContextCompat.getColor(context, R.color.global);
        mFollowCancelColor = 0xffb3b3b3;
        mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                UserBean bean = (UserBean) tag;
                if (mActionListener != null) {
                    mActionListener.onItemClick(bean);
                }
            }
        };
        mFollowClickListner = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!canClick()) {
                    return;
                }
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                UserBean bean = (UserBean) tag;
                if (mActionListener != null) {
                    mActionListener.onFollowClick(bean);
                }
            }
        };
        mIdString = WordUtil.getString(R.string.search_id);
        mFanString = WordUtil.getString(R.string.search_fans);
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(mInflater.inflate(R.layout.item_follow, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position, @NonNull List payloads) {
        Object payload = payloads.size() > 0 ? payloads.get(0) : null;
        ((Vh) vh).setData(mList.get(position), payload);
    }

    public void updateItem(String id, int attention) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        for (int i = 0, size = mList.size(); i < size; i++) {
            UserBean bean = mList.get(i);
            if (bean != null && id.equals(bean.getId())) {
                bean.setAttent(attention);
                notifyItemChanged(i, Constants.PAYLOAD);
                break;
            }
        }
    }


    class Vh extends RecyclerView.ViewHolder {

        ImageView mAvatar;
        TextView mName;
        TextView mID;
        TextView mFans;
        TextView mBtnFollow;

        public Vh(View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mID = itemView.findViewById(R.id.id_val);
            mFans = itemView.findViewById(R.id.fans);
            mBtnFollow = itemView.findViewById(R.id.btn_follow);
            itemView.setOnClickListener(mClickListener);
            mBtnFollow.setOnClickListener(mFollowClickListner);
        }

        void setData(UserBean bean, Object payload) {
            itemView.setTag(bean);
            mBtnFollow.setTag(bean);
            if (payload == null) {
                ImgLoader.displayAvatar(mContext, bean.getAvatar(), mAvatar);
                mName.setText(bean.getUserNiceName());
                mID.setText(StringUtil.contact(mIdString, bean.getId()));
                mFans.setText(StringUtil.contact(mFanString, StringUtil.toWan(bean.getFans())));
            }
            if (bean.isFollowing()) {
                mBtnFollow.setText(mFollowCancelString);
                mBtnFollow.setBackground(mFollowCancelDrawable);
                mBtnFollow.setTextColor(mFollowCancelColor);
            } else {
                mBtnFollow.setText(mFollowAddString);
                mBtnFollow.setBackground(mFollowAddDrawable);
                mBtnFollow.setTextColor(mFollowAddColor);
            }
        }
    }


    public interface ActionListener {
        void onItemClick(UserBean userBean);

        void onFollowClick(UserBean userBean);
    }


    public void setActionListener(ActionListener actionListener) {
        mActionListener = actionListener;
    }
}
