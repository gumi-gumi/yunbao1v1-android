package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.BitmapList;
import com.yunbao.common.utils.ClickUtil;

import com.yunbao.common.utils.FileUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.bean.AuthBean;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class AuthActivity extends AbsActivity{

    public static void forward(Context context) {
        Intent intent = new Intent(context, AuthActivity.class);
        context.startActivity(intent);
    }
    private RelativeLayout mShiMing,mZhuBo;
    private ImageView mAuth,mAuthor,image1,image2;
    private String status_user,status_author,name,cardNo,phone,thumb,video,videoFormat,videoThumb;
    private List<String> backWall;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_auth;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.auth_my_auth));
        mShiMing=findViewById(R.id.rl_shiming);
        mZhuBo=findViewById(R.id.rl_zhubo);
        mAuth=findViewById(R.id.img_auth);
        mAuthor=findViewById(R.id.img_author);
        image1=findViewById(R.id.img1);
        image2=findViewById(R.id.img2);
        getAuthStatus();//获取认证状态
    }

    public void authClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.rl_shiming) {
            if ("1".equals(status_user)){//已认证
                MyAuthActivity.forward(mContext,name,cardNo,phone);
            }else if ("-1".equals(status_user)){
                RealNameActivity.forward(mContext);
            }else{
                RealNameCheckActivity.forward(mContext,name,cardNo,phone,status_user);
            }
        }else if (i == R.id.rl_zhubo) {
            if (!"1".equals(status_user)) {
                ToastUtil.show(R.string.auth_no);
            } else {
                for (int p = 0; p< BitmapList.list.size(); p++) {
                    for (int j = 0; j < BitmapList.list.size(); j++) {
                        if (i == j) {
                            continue;
                        }
                        if (BitmapList.list.get(p).equals(BitmapList.list.get(j))&&BitmapList.list.size()>backWall.size()) {
                            BitmapList.list.remove(j);
                        }
                    }
                }
                AnchorAuthActivity.forward(mContext,videoFormat,video,status_author);
            }
        }
    }

    private void getAuthStatus(){
        MainHttpUtil.getAuthStatus(new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                System.out.print(info[0]);
                Gson gson=new Gson();
                AuthBean authBean= gson.fromJson(info[0],AuthBean.class);
                List<AuthBean.AuthInfoBean> infoBeanList=authBean.getAuth_info();
                 status_user=infoBeanList.get(0).getStatus();
                 status_author=infoBeanList.get(1).getStatus();
                 name=infoBeanList.get(0).getName();
                 cardNo=infoBeanList.get(0).getCardno();
                 phone=infoBeanList.get(0).getMobile();
                 thumb=infoBeanList.get(1).getThumb_format();
                 backWall=infoBeanList.get(1).getBackwall_list_format();
                 video=infoBeanList.get(1).getVideo();
                 videoFormat=infoBeanList.get(1).getVideo_format();
                 videoThumb=infoBeanList.get(1).getVideo_thumb_format();
                  ImgLoader.display(mContext, infoBeanList.get(0).getAuth_icon(), image1);
                  ImgLoader.display(mContext, infoBeanList.get(1).getAuth_icon(), image2);

                // 启动线程
                MyThread myThread = new MyThread();
                myThread.start();

               if ("-1".equals(status_user)||"0".equals(status_user)||"2".equals(status_user)){//实名认证未通过
                   mAuth.setImageResource(R.mipmap.weirenzheng);
               }else{
                   mAuth.setImageResource(R.mipmap.yirenzheng);
               }
                if ("-1".equals(status_author)||"0".equals(status_author)||"2".equals(status_author)){//实名认证未通过
                    mAuthor.setImageResource(R.mipmap.weirenzheng);
                }else{
                    mAuthor.setImageResource(R.mipmap.yirenzheng);
                }




            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAuthStatus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainHttpUtil.cancel(MainHttpConsts.GET_AUTH_STATUS);
    }

    class MyThread extends Thread {
        @Override
        public void run() {
            BitmapList.sBitmap = FileUtil.getBitmap(thumb);
            BitmapList.sBitmap1=FileUtil.getBitmap(videoThumb);
            BitmapList.list.clear();
            for (int i=0;i<backWall.size();i++){
                BitmapList.list.add(FileUtil.getBitmap(backWall.get(i)));

            }


        }
    }



}
