package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.main.R;
import com.yunbao.main.adapter.SubcribeAudAdapter;
import com.yunbao.main.bean.SubcribeAudBean;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 我预约的
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class SubcribeAudViewHolder extends AbsMainViewHolder implements OnItemClickListener<SubcribeAudBean> {

    private CommonRefreshView mRefreshView;
    private SubcribeAudAdapter mAdapter;

    public SubcribeAudViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_subcribe_aud;
    }

    @Override
    public void init() {
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        mRefreshView.setEmptyLayoutId(R.layout.view_no_data_subcribe_1);
        mRefreshView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<SubcribeAudBean>() {
            @Override
            public RefreshAdapter<SubcribeAudBean> getAdapter() {
                if (mAdapter == null) {
                    mAdapter = new SubcribeAudAdapter(mContext);
                    mAdapter.setOnItemClickListener(SubcribeAudViewHolder.this);
                }
                return mAdapter;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                OneHttpUtil.getMySubscribeList(p, callback);
            }

            @Override
            public List<SubcribeAudBean> processData(String[] info) {
                return JSON.parseArray(Arrays.toString(info), SubcribeAudBean.class);
            }

            @Override
            public void onRefreshSuccess(List<SubcribeAudBean> list, int listCount) {

            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<SubcribeAudBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
    }

    @Override
    public void loadData() {
        if (isFirstLoadData()) {
            if (mRefreshView != null) {
                mRefreshView.initData();
            }
        }
    }

    @Override
    public void onDestroy() {
        OneHttpUtil.cancel(OneHttpConsts.GET_MY_SUBCRIBE_LIST);
        super.onDestroy();
    }

    @Override
    public void onItemClick(SubcribeAudBean bean, int position) {
        RouteUtil.forwardUserHome(bean.getUid());
    }
}
