package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.main.R;
import com.yunbao.main.views.MainListViewHolder;

import net.lucode.hackware.magicindicator.MagicIndicator;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RankListActivity extends AbsActivity{
    private MainListViewHolder mMainListViewHolder;
    private int mDefault;
    public static void forward(Context context,int defaultPos){
        if(!ClickUtil.canClick()){
            return;
        }
        Intent intent=new Intent(context,RankListActivity.class);
        intent.putExtra(Constants.RANK_DEFAULT,defaultPos);
        context.startActivity(intent);
    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_rank_list;
    }

    @Override
    protected void main() {
        super.main();
        mDefault=getIntent().getIntExtra(Constants.RANK_DEFAULT,0);
        mMainListViewHolder=new MainListViewHolder(mContext, (ViewGroup) findViewById(R.id.container),mDefault);
        mMainListViewHolder.addToParent();
        mMainListViewHolder.subscribeActivityLifeCycle();
        mMainListViewHolder.loadData();
    }
}
