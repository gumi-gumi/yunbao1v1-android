package com.yunbao.main.custom;

import android.annotation.SuppressLint;
import android.widget.TextView;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import com.yunbao.main.R;

@SuppressLint("AppCompatCustomView")
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class CustomProgressBar extends TextView {
    private int lowColor;
    private int normalColor;
    private Paint mPaint;
    /** 百分比 */
    private int mRate;
    /*** 百分比的数值 */
    private int mRateText;
    /** 外边框的宽度 */
    private int mBorder = 2;
    /*** 柱体的宽 **/
    private int mW;
    /*** 间隔水平距离 */
    private int mDividerW = 2;
    /*** 柱体个数默认10 **/
    private int mSum = 80;


    /*** 分割后有一个未完整部分 均分到 柱体的左右两边，使柱体居中 */
    private int mRemain;


    /***
     * 100%分割成十个柱体 每个代表 10% 那么有且最多只有一个是不满的 如76% 那么前7（mCount）个是满的都是 10% 第八个只有6成满
     **/
    private int mCount;
    /*** 不满的那一格的数值 */
    private int mBoundRate;


    public CustomProgressBar(Context context) {
        super(context);
    }


    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }


    public CustomProgressBar(Context context, AttributeSet attrs,
                             int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }


    @SuppressLint("ResourceAsColor")
    private void init(Context context, AttributeSet attrs, int defStyle) {
/**
 * 获得我们所定义的自定义样式属性
 */
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CustomProgressBar, defStyle, 0);
        lowColor = a.getColor(R.styleable.CustomProgressBar_lowColor, R.color.purple);
        normalColor = a.getColor(R.styleable.CustomProgressBar_normalColor, R.color.textColor2);
        String text = a.getString(R.styleable.CustomProgressBar_rate);
        try {
            mRate = Integer.parseInt(text);
        } catch (NumberFormatException e) {
            mRate = 0;
        }
        mCount = mRate / 80;
        mBoundRate = mRate % 80;
        mRateText = mRate;
        a.recycle();
        mPaint = new Paint();
        mPaint.setTextSize(getTextSize());
        mPaint.setColor(normalColor);
        Log.e("zjs", "mCount==" + mCount + "  mBoundRate=" + mBoundRate);


    }

    public int getRate(){
        return mRate;
    }


    public void setRate(int rate) {
        mRateText = rate;
        mRate = rate;
        mCount = mRate / 80;
        mBoundRate = mRate % 80;
        this.invalidate();


    }


    @Override
    protected void onDraw(Canvas canvas) {
        //drawRectBorder(canvas);
        drawRects(canvas);
        drawRate(canvas);
        super.onDraw(canvas);
    }


    /*** 绘制10个柱体 */
    private void drawRects(Canvas canvas) {
        Paint paint = new Paint();
        Paint paint1 = new Paint();
        paint.setColor(normalColor);

        paint1.setColor(lowColor);
        mW = (getWidth() - mDividerW - mBorder) / mSum - mDividerW;
        mRemain = mBorder / 2;
        float x = 0f;
        for (int i = 0; i < mCount; i++) {
            x = getWidth() - ((mW + mDividerW) * i + mDividerW + mRemain);
                canvas.drawRect(x - mW, mBorder, x, getHeight() - mBorder, paint1);
        }
        x = getWidth() - ((mW + mDividerW) * mCount + 1 + mDividerW + mRemain);// (mW+mDividerW)*+mCount+1+mDividerW+mW+mRemain
        int y = getHeight() - ((getHeight() - mBorder - mBorder) * mBoundRate / 80) - mBorder;// -mBorder
        canvas.drawRect(x - mW, y, x, getHeight() - mBorder, paint);
    }


    /*** 绘制外边框 **/
    private void drawRectBorder(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), paint);


// 设置空心Style
        mPaint.setStyle(Paint.Style.STROKE);
// 设置空心边框的宽度
        mPaint.setStrokeWidth(mBorder);
// 绘制空心矩形
        canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);
    }


    /*** 绘制百分比的文本 */
    private void drawRate(Canvas canvas) {
        Log.e("zjs", "mCount==" + mCount + "  mBoundRate=" + mBoundRate);
        Paint paint = new Paint();
// 去锯齿
        paint.setAntiAlias(true);
// 设置颜色
        paint.setColor(Color.WHITE);
        paint.setTextSize(getTextSize());
// 绘制文本
        canvas.drawText(mRateText + "%", getWidth() / 2, getHeight() / 2
                + mBorder, paint);
    }


}
