package com.yunbao.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.CommonIconUtil;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.utils.MainIconUtil;
import com.yunbao.one.bean.ChatLiveBean;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainHomeUserAdapter extends RefreshAdapter<UserBean> {

    private View.OnClickListener mOnClickListener;

    public MainHomeUserAdapter(Context context) {
        super(context);
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag != null) {
                    int position = (int) tag;
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(mList.get(position), position);
                    }
                }
            }
        };

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(mInflater.inflate(R.layout.item_main_home_user, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {
        ((Vh) vh).setData(mList.get(position), position);
    }

    class Vh extends RecyclerView.ViewHolder {

        ImageView mAvatar;
        TextView mName;
        ImageView mSex;
        ImageView mHaveAuth;
        ImageView mNotAuth;
        ImageView mOnLine;

        public Vh(View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.tv_name);
            mSex = itemView.findViewById(R.id.sex);
            mHaveAuth = itemView.findViewById(R.id.have_auth);
            mNotAuth = itemView.findViewById(R.id.not_auth);
            mOnLine = itemView.findViewById(R.id.on_line);
            itemView.setOnClickListener(mOnClickListener);
        }

        void setData(UserBean bean, int position) {
            itemView.setTag(position);
            ImgLoader.display(mContext, bean.getAvatar(), mAvatar);
            mName.setText(bean.getUserNiceName());
            if (bean.getIsAuthorAuth() == 1) {
                mHaveAuth.setVisibility(View.VISIBLE);
                mNotAuth.setVisibility(View.GONE);
            } else {
                mHaveAuth.setVisibility(View.GONE);
                mNotAuth.setVisibility(View.VISIBLE);
            }
            mOnLine.setImageResource(CommonIconUtil.getOnLineIcon3(bean.getOnLineStatus()));
            mSex.setImageResource(CommonIconUtil.getSexIcon(bean.getSex()));
        }
    }
}
