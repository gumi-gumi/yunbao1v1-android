package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.ChatAnchorParam;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.main.R;
import com.yunbao.main.adapter.SubcribeAncAdapter;
import com.yunbao.main.bean.SubcribeAncBean;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 预约我的
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class SubcribeAncViewHolder extends AbsMainViewHolder implements SubcribeAncAdapter.ActionListener {

    private CommonRefreshView mRefreshView;
    private SubcribeAncAdapter mAdapter;
    private HttpCallback mAncToAudCallback;
    private SubcribeAncBean mSubcribeAncBean;

    public SubcribeAncViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_subcribe_aud;
    }

    @Override
    public void init() {
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        mRefreshView.setEmptyLayoutId(R.layout.view_no_data_subcribe_2);
        mRefreshView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<SubcribeAncBean>() {
            @Override
            public RefreshAdapter<SubcribeAncBean> getAdapter() {
                if (mAdapter == null) {
                    mAdapter = new SubcribeAncAdapter(mContext);
                    mAdapter.setActionListener(SubcribeAncViewHolder.this);
                }
                return mAdapter;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                OneHttpUtil.getSubscribeMeList(p, callback);
            }

            @Override
            public List<SubcribeAncBean> processData(String[] info) {
                return JSON.parseArray(Arrays.toString(info), SubcribeAncBean.class);
            }

            @Override
            public void onRefreshSuccess(List<SubcribeAncBean> list, int listCount) {

            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<SubcribeAncBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
    }

    @Override
    public void loadData() {
        if (isFirstLoadData()) {
            if (mRefreshView != null) {
                mRefreshView.initData();
            }
        }
    }

    @Override
    public void onDestroy() {
        OneHttpUtil.cancel(OneHttpConsts.GET_SUBCRIBE_ME_LIST);
        OneHttpUtil.cancel(OneHttpConsts.CHAT_ANC_TO_AUD_START);
        super.onDestroy();
    }

    @Override
    public void onItemClick(SubcribeAncBean u) {
        RouteUtil.forwardUserHome(u.getUid());
    }

    @Override
    public void onToSubcribeClick(SubcribeAncBean u) {
        mSubcribeAncBean = u;
        if (mAncToAudCallback == null) {
            mAncToAudCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        if (info.length > 0 && mSubcribeAncBean != null) {
                            JSONObject obj = JSON.parseObject(info[0]);
                            ChatAnchorParam param = new ChatAnchorParam();
                            param.setAudienceID(mSubcribeAncBean.getUid());
                            param.setAudienceName(mSubcribeAncBean.getUserNiceName());
                            param.setAudienceAvatar(mSubcribeAncBean.getAvatar());
                            param.setSessionId(obj.getString("showid"));
                            param.setAnchorPlayUrl(obj.getString("pull"));
                            param.setAnchorPushUrl(obj.getString("push"));
                            param.setPrice(obj.getString("total"));
                            param.setChatType(obj.getIntValue("type"));
                            param.setAnchorActive(true);
                            RouteUtil.forwardAnchorActivity(param);
                        }
                    } else {
                        ToastUtil.show(msg);
                    }
                }
            };
        }
        OneHttpUtil.chatAncToAudStart(u.getSubscribeId(), mAncToAudCallback);
    }
}
