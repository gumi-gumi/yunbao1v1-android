package com.yunbao.main.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.dialog.AbsDialogFragment;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.DpUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.MainActivity;
import com.yunbao.one.activity.MatchAnchorActivity;
import com.yunbao.one.activity.MatchAudienceActivity;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MatchDialogFragment extends AbsDialogFragment implements View.OnClickListener {

    private TextView mContent;
    private boolean mHasAuth;
    private int mType;
    private String mVoicePrice;
    private String mVoicePriceVip;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_match;
    }

    @Override
    protected int getDialogStyle() {
        return R.style.dialog3;
    }

    @Override
    protected boolean canCancel() {
        return true;
    }

    @Override
    protected void setWindowAttributes(Window window) {
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = DpUtil.dp2px(280);
        params.height = DpUtil.dp2px(150);
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        mType = bundle.getInt(Constants.CHAT_TYPE);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
        mContent = (TextView) findViewById(R.id.content);
        OneHttpUtil.getMatchInfo(new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0) {
                    JSONObject obj = JSON.parseObject(info[0]);
                    mHasAuth = obj.getIntValue("isauth") == 1;
                    mVoicePrice = obj.getString("voice");
                    mVoicePriceVip = obj.getString("voice_vip");
                    String coinName = CommonAppConfig.getInstance().getCoinName();
                    String s = String.format(WordUtil.getString(R.string.a_003), mVoicePrice, coinName);
                    if (mContent != null) {
                        mContent.setText(s);
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_cancel) {
            dismissAllowingStateLoss();
        } else if (i == R.id.btn_confirm) {
            forwardMatch();
            dismissAllowingStateLoss();
        }
    }

    private void forwardMatch() {
        if (mHasAuth) {
            MatchAnchorActivity.forward(mContext, mType);
        } else {
            OneHttpUtil.matchCheck(mType, new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        MatchAudienceActivity.forward(mContext, mVoicePrice, mVoicePriceVip, Constants.CHAT_TYPE_VOICE);
                    } else if (code == 1008) {
                        ToastUtil.show(R.string.chat_coin_not_enough);
                        ((MainActivity) mContext).openChargeWindow();
                    } else {
                        ToastUtil.show(msg);
                    }
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        OneHttpUtil.cancel(OneHttpConsts.GET_MATCH_INFO);
        super.onDestroy();
    }
}
