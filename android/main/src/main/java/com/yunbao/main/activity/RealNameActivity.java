package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.main.R;
import com.yunbao.main.R2;
import com.yunbao.main.http.MainHttpUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RealNameActivity extends AbsActivity implements View.OnClickListener {

    @BindView(R2.id.titleView)
    TextView titleView;
    @BindView(R2.id.btn_back)
    ImageView btnBack;
    @BindView(R2.id.et_name)
    EditText etName;
    @BindView(R2.id.et_cd)
    EditText etCd;
    @BindView(R2.id.et_phone)
    EditText etPhone;
    @BindView(R2.id.btn_submit)
    Button btnSubmit;

    public static void forward(Context context) {
        Intent intent = new Intent(context,RealNameActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_real_name;
    }

    @Override
    protected void main() {

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_submit) {
            if (TextUtils.isEmpty(etName.getText())){
                ToastUtil.show(R.string.name_no);
            }else if (TextUtils.isEmpty(etCd.getText())){
               ToastUtil.show(R.string.card_no);
            }else if (TextUtils.isEmpty(etPhone.getText())){
                ToastUtil.show(R.string.mobile_no);
            }else {
                submit();//提交认证
            }

        }
    }
    /**
     * 实名认证
     */
    private void submit(){
        MainHttpUtil.setUserAuth(etName.getText().toString(), etPhone.getText().toString(), etCd.getText().toString(), new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if(code==0){
                    ToastUtil.show(msg);
                    finish();
                }else{
                    ToastUtil.show(msg);
                }

            }
        });
    }
}
