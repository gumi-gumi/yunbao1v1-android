package com.yunbao.main.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.DpUtil;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.utils.MainIconUtil;
import com.yunbao.one.bean.ChatLiveBean;

import java.util.List;

/**
 * 首页 推荐
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainHomeRecommendAdapter extends RefreshAdapter<ChatLiveBean> {

    private static final int HEAD = 0;
    private static final int LEFT = 1;
    private static final int RIGHT = 2;
    private View.OnClickListener mOnClickListener;
    private View mHeadView;
    private Drawable mVideoDrawable;
    private Drawable mVoiceDrawable;
    private String mPriceSuffix;

    public MainHomeRecommendAdapter(Context context) {
        super(context);
        mHeadView = mInflater.inflate(R.layout.item_main_home_live_head, null, false);
        mHeadView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag != null) {
                    int position = (int) tag;
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(mList.get(position), position);
                    }
                }
            }
        };
        mVideoDrawable = ContextCompat.getDrawable(context, R.mipmap.o_main_price_video);
        mVoiceDrawable = ContextCompat.getDrawable(context, R.mipmap.o_main_price_voice);
        mPriceSuffix = String.format(WordUtil.getString(R.string.main_price), CommonAppConfig.getInstance().getCoinName());
    }

    public View getHeadView() {
        return mHeadView;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEAD;
        } else if (position % 2 == 0) {
            return RIGHT;
        }
        return LEFT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEAD) {
            ViewParent viewParent = mHeadView.getParent();
            if (viewParent != null) {
                ((ViewGroup) viewParent).removeView(mHeadView);
            }
            HeadVh headVh = new HeadVh(mHeadView);
            headVh.setIsRecyclable(false);
            return headVh;
        } else if (viewType == LEFT) {
            return new Vh(mInflater.inflate(R.layout.item_main_home_live_left, parent, false));
        }
        return new Vh(mInflater.inflate(R.layout.item_main_home_live_right, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {
        if (vh instanceof Vh) {
            ((Vh) vh).setData(mList.get(position - 1), position - 1);
        }
    }

    @Override
    public void insertList(List<ChatLiveBean> list) {
        if (mRecyclerView != null && mList != null && list != null && list.size() > 0) {
            int p = mList.size();
            mList.addAll(list);
            notifyItemRangeInserted(p + 1, list.size());
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    class HeadVh extends RecyclerView.ViewHolder {

        public HeadVh(View itemView) {
            super(itemView);
        }
    }

    class Vh extends RecyclerView.ViewHolder {

        ImageView mCover;
        ImageView mPriceIcon;
        ImageView mLevel;
        View mVideoIcon;
        View mVoiceIcon;
        TextView mPrice;
        TextView mName;
        ImageView mOnLine;

        public Vh(View itemView) {
            super(itemView);
            mCover = itemView.findViewById(R.id.cover);
            mPriceIcon = itemView.findViewById(R.id.price_icon);
            mLevel = itemView.findViewById(R.id.level);
            mVideoIcon = itemView.findViewById(R.id.video);
            mVoiceIcon = itemView.findViewById(R.id.voice);
            mPrice = itemView.findViewById(R.id.price);
            mName = itemView.findViewById(R.id.name);
            mOnLine = itemView.findViewById(R.id.on_line);
            itemView.setOnClickListener(mOnClickListener);
        }

        void setData(ChatLiveBean bean, int position) {
            itemView.setTag(position);
            ImgLoader.display(mContext, bean.getThumb(), mCover);
            mName.setText(bean.getUserNiceName());
            mOnLine.setImageResource(MainIconUtil.getOnLineIcon1(bean.getOnLineStatus()));
            if (bean.isOpenVideo()) {
                if (mVideoIcon.getVisibility() != View.VISIBLE) {
                    mVideoIcon.setVisibility(View.VISIBLE);
                }
                mPriceIcon.setImageDrawable(mVideoDrawable);
                mPrice.setText(StringUtil.contact(bean.getPriceVideo(), mPriceSuffix));
            } else {
                if (mVideoIcon.getVisibility() == View.VISIBLE) {
                    mVideoIcon.setVisibility(View.GONE);
                }
                if (bean.isOpenVoice()) {
                    mPriceIcon.setImageDrawable(mVoiceDrawable);
                    mPrice.setText(StringUtil.contact(bean.getPriceVoice(), mPriceSuffix));
                } else {
                    mPriceIcon.setImageDrawable(mVideoDrawable);
                    mPrice.setText(StringUtil.contact(bean.getPriceVideo(), mPriceSuffix));
                }
            }
            if (bean.isOpenVoice()) {
                if (mVoiceIcon.getVisibility() != View.VISIBLE) {
                    mVoiceIcon.setVisibility(View.VISIBLE);
                }
            } else {
                if (mVoiceIcon.getVisibility() == View.VISIBLE) {
                    mVoiceIcon.setVisibility(View.GONE);
                }
            }

        }

    }


}
