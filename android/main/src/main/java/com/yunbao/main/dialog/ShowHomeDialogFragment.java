package com.yunbao.main.dialog;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.yunbao.common.Constants;
import com.yunbao.common.dialog.AbsDialogFragment;
import com.yunbao.main.R;
import com.yunbao.main.views.UserHomeUserViewHolder;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class ShowHomeDialogFragment extends AbsDialogFragment {

    private UserHomeUserViewHolder homeFirst2ViewHolder;
    private FrameLayout mRoot;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_show_home;
    }

    @Override
    protected int getDialogStyle() {
        return R.style.dialog;
    }

    @Override
    protected boolean canCancel() {
        return false;
    }

    @Override
    protected void setWindowAttributes(Window window) {
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle == null) {
                return;
            }
            String toUid = bundle.getString(Constants.TO_UID);
            int pos = bundle.getInt(Constants.POSITION, 0);
            mRoot = (FrameLayout) findViewById(R.id.fl_root);
            homeFirst2ViewHolder = new UserHomeUserViewHolder(mContext, mRoot, toUid, pos, this);
            homeFirst2ViewHolder.addToParent();
            homeFirst2ViewHolder.subscribeActivityLifeCycle();
            homeFirst2ViewHolder.loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (homeFirst2ViewHolder != null) {
            homeFirst2ViewHolder.setVideoPause(true);
            homeFirst2ViewHolder.removeFromParent();
        }
        homeFirst2ViewHolder = null;
        if (mListener != null) {
            mListener.onDismiss();
        }
    }

    public interface IOnDialogDismissListener {
        void onDismiss();
    }

    private IOnDialogDismissListener mListener;

    public void setOnDialogDismissListener(IOnDialogDismissListener listener) {
        this.mListener = listener;
    }
}
