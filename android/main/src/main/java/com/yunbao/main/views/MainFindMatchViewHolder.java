package com.yunbao.main.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.HtmlConfig;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.pay.PayCallback;
import com.yunbao.common.pay.PayPresenter;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.one.activity.MatchAnchorActivity;
import com.yunbao.one.activity.MatchAudienceActivity;
import com.yunbao.one.dialog.ChatChargeDialogFragment;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;

import pl.droidsonroids.gif.GifImageView;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainFindMatchViewHolder extends AbsMainViewHolder implements View.OnClickListener {

    private GifImageView mGifImageView;
    private TextView mBtnVideo;
    private TextView mBtnVoice;
    private TextView mPrice;
    private TextView mPriceVip;
    private TextView mQueueCount;
    private int mType;
    private Drawable mRadioBg;
    private int mCheckedColor;
    private int mUnCheckedColor;
    private String mCoinName;
    private PayPresenter mPayPresenter;
    private boolean mHasAuth;
    private String mQueueCountString;
    private HttpCallback mMatchInfoCallback;
    private HttpCallback mMatchCheckCallback;
    private String mVideoPrice;
    private String mVideoPriceVip;
    private String mVoicePrice;
    private String mVoicePriceVip;

    public MainFindMatchViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_find_macth;
    }

    @Override
    public void init() {
        mCoinName = CommonAppConfig.getInstance().getCoinName();
        mQueueCountString = WordUtil.getString(R.string.match_queue_count);
        mRadioBg = ContextCompat.getDrawable(mContext, R.drawable.bg_main_find_check);
        mCheckedColor = ContextCompat.getColor(mContext, R.color.white);
        mUnCheckedColor = ContextCompat.getColor(mContext, R.color.textColor);
        mType = Constants.CHAT_TYPE_VIDEO;
        mGifImageView = (GifImageView) findViewById(R.id.gif);
        mQueueCount = (TextView) findViewById(R.id.queue_count);
        mBtnVideo = (TextView) findViewById(R.id.btn_video);
        mBtnVoice = (TextView) findViewById(R.id.btn_voice);
        showType();
        mBtnVideo.setOnClickListener(this);
        mBtnVoice.setOnClickListener(this);
        findViewById(R.id.btn_match).setOnClickListener(this);
        mPrice = (TextView) findViewById(R.id.price);
        mPriceVip = (TextView) findViewById(R.id.price_vip);
    }

    /**
     * 选择 视频 语音 类型
     */
    private void showType() {
        if (mType == Constants.CHAT_TYPE_VIDEO) {
            mBtnVideo.setBackground(mRadioBg);
            mBtnVideo.setTextColor(mCheckedColor);
            mBtnVoice.setBackground(null);
            mBtnVoice.setTextColor(mUnCheckedColor);
        } else if (mType == Constants.CHAT_TYPE_VOICE) {
            mBtnVideo.setBackground(null);
            mBtnVideo.setTextColor(mUnCheckedColor);
            mBtnVoice.setBackground(mRadioBg);
            mBtnVoice.setTextColor(mCheckedColor);
        }
    }

    private void showPrice() {
        if (mType == Constants.CHAT_TYPE_VIDEO) {
            if (mPrice != null) {
                mPrice.setText(mVideoPrice);
                mPriceVip.setText(mVideoPriceVip);
            }
        } else if (mType == Constants.CHAT_TYPE_VOICE) {
            if (mPrice != null) {
                mPrice.setText(mVoicePrice);
                mPriceVip.setText(mVoicePriceVip);
            }
        }
    }


    /**
     * 显示排队人数
     */
    private void showQueueCount(String count) {
        if (mQueueCount != null) {
            mQueueCount.setText(StringUtil.contact(mQueueCountString, count));
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_video) {
            clickVideo();

        } else if (i == R.id.btn_voice) {
            clickVoice();
        } else if (i == R.id.btn_match) {
            clickMatch();
        }
    }

    private void clickVideo() {
        if (mType == Constants.CHAT_TYPE_VIDEO) {
            return;
        }
        mType = Constants.CHAT_TYPE_VIDEO;
        showType();
        showPrice();
    }


    private void clickVoice() {
        if (mType == Constants.CHAT_TYPE_VOICE) {
            return;
        }
        mType = Constants.CHAT_TYPE_VOICE;
        showType();
        showPrice();
    }

    private void clickMatch() {
        //openChargeWindow();
        if (mHasAuth) {
            MatchAnchorActivity.forward(mContext, mType);
        } else {
            matchCheck();
        }
    }


    private void matchCheck() {
        if (mMatchCheckCallback == null) {
            mMatchCheckCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        if (mType == Constants.CHAT_TYPE_VIDEO) {
                            MatchAudienceActivity.forward(mContext, mVideoPrice, mVideoPriceVip, Constants.CHAT_TYPE_VIDEO);
                        } else {
                            MatchAudienceActivity.forward(mContext, mVoicePrice, mVoicePriceVip, Constants.CHAT_TYPE_VOICE);
                        }
                    } else if (code == 1008) {
                        ToastUtil.show(R.string.chat_coin_not_enough);
                        openChargeWindow();
                    } else {
                        ToastUtil.show(msg);
                    }
                }
            };
        }
        OneHttpUtil.matchCheck(mType, mMatchCheckCallback);
    }



    /**
     * 打开充值窗口
     */
    public void openChargeWindow() {
        if (mPayPresenter == null) {
            mPayPresenter = new PayPresenter((AbsActivity) mContext);
            mPayPresenter.setServiceNameAli(Constants.PAY_BUY_COIN_ALI);
            mPayPresenter.setServiceNameWx(Constants.PAY_BUY_COIN_WX);
            mPayPresenter.setAliCallbackUrl(HtmlConfig.ALI_PAY_COIN_URL);
            mPayPresenter.setPayCallback(new PayCallback() {
                @Override
                public void onSuccess() {
                    if (mPayPresenter != null) {
                        mPayPresenter.checkPayResult();
                    }
                }

                @Override
                public void onFailed() {

                }
            });
        }
        ChatChargeDialogFragment fragment = new ChatChargeDialogFragment();
        fragment.setPayPresenter(mPayPresenter);
        fragment.show(((AbsActivity) mContext).getSupportFragmentManager(), "ChatChargeDialogFragment");
    }


    @Override
    public void loadData() {
        if (mMatchInfoCallback == null) {
            mMatchInfoCallback = new HttpCallback() {
                @Override
                public void onSuccess(int code, String msg, String[] info) {
                    if (code == 0) {
                        JSONObject obj = JSON.parseObject(info[0]);
                        //mHasAuth = obj.getIntValue("isauth") == 1;
                        mHasAuth = obj.getIntValue("isauthor_auth") == 1;
                        if (mGifImageView != null && mGifImageView.getDrawable() == null) {
                            mGifImageView.setImageResource(mHasAuth ? R.mipmap.o_find_match_bg_2 : R.mipmap.o_find_match_bg_1);
                        }
                        mVideoPrice = String.format(WordUtil.getString(R.string.match_price), obj.getString("video"), mCoinName);
                        mVideoPriceVip = String.format(WordUtil.getString(R.string.match_price_vip), obj.getString("video_vip"), mCoinName);
                        mVoicePrice = String.format(WordUtil.getString(R.string.match_price), obj.getString("voice"), mCoinName);
                        mVoicePriceVip = String.format(WordUtil.getString(R.string.match_price_vip), obj.getString("voice_vip"), mCoinName);
                        showPrice();
                    }
                }
            };
        }
        OneHttpUtil.getMatchInfo(mMatchInfoCallback);
    }

    @Override
    public void onDestroy() {
        OneHttpUtil.cancel(OneHttpConsts.GET_MATCH_INFO);
        super.onDestroy();
    }
}
