package com.yunbao.main.activity;

import android.view.View;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.event.CoinChangeEvent;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class WalletActivity extends AbsActivity {

    private TextView mCoinName;
    private TextView mCoin;
    private boolean mPaused;
    private boolean mCoinChanged;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wallet;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.wallet));
        mCoinName = findViewById(R.id.coin_name);
        mCoin = findViewById(R.id.coin);
        mCoinName.setText(String.format(WordUtil.getString(R.string.wallet_coin_name), CommonAppConfig.getInstance().getCoinName()));
        EventBus.getDefault().register(this);
        getLastCoin();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPaused && mCoinChanged) {
            mCoinChanged = false;
            getLastCoin();
        }
        mPaused = false;
    }

    /**
     * 获取余额
     */
    private void getLastCoin() {
        MainHttpUtil.getBaseInfo(new CommonCallback<Object[]>() {
            @Override
            public void callback(Object[] bean) {
                if (bean[0] != null && mCoin != null) {
                    mCoin.setText(((UserBean) bean[0]).getCoin());
                }
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCoinChangeEvent(CoinChangeEvent e) {
        mCoinChanged = true;
    }

    public void walletClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.btn_charge) {
            RouteUtil.forwardMyCoin();
        } else if (i == R.id.btn_detail) {
            WalletDetailActivity.forward(mContext);
        } else if (i == R.id.btn_profit) {
            MyProfitActivity.forward(mContext);
        }
    }


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        MainHttpUtil.cancel(MainHttpConsts.GET_BASE_INFO);
        super.onDestroy();
    }
}
