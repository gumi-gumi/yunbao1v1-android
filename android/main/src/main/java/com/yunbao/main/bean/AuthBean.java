package com.yunbao.main.bean;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class AuthBean {


    /**
     * auth_info : [{"addtime":"2021-12-07 10:05:36","auth_icon":"http://live1v1test2.yunbaozhibo.com/static/app/auth/user_auth.png","auth_title":"实名认证","auth_type":"user_auth","cardno":"370911199105087230","mobile":"15588527855","name":"车德东","reason":" ","status":"1","uid":"103058","uptime":"2021-12-09 14:27:37"},{"addtime":"2021-12-09 17:25:29","auth_icon":"http://live1v1test2.yunbaozhibo.com/static/app/auth/author_auth.png","auth_title":"主播认证","auth_type":"author_auth","backwall":"qiniu_android_103058_20211209_172527_8954863.png","backwall_list":["qiniu_android_103058_20211209_172527_8954863.png"],"backwall_list_format":["http://yb1v1.yunbaozb.com/android_103058_20211209_172527_8954863.png"],"id":"6","reason":"","status":"0","thumb":"qiniu_android_103058_20211209_172527_6771862.png","thumb_format":"http://yb1v1.yunbaozb.com/android_103058_20211209_172527_6771862.png","uid":"103058","uptime":"0","video":"qiniu_android_103058_20211209_172528_0436775.mp4","video_format":"http://yb1v1.yunbaozb.com/android_103058_20211209_172528_0436775.mp4","video_thumb":"","video_thumb_format":""}]
     * author_auth_status : 0
     * user_auth_status : 1
     */

    private String author_auth_status;
    private String user_auth_status;
    private List<AuthInfoBean> auth_info;

    public String getAuthor_auth_status() {
        return author_auth_status;
    }

    public void setAuthor_auth_status(String author_auth_status) {
        this.author_auth_status = author_auth_status;
    }

    public String getUser_auth_status() {
        return user_auth_status;
    }

    public void setUser_auth_status(String user_auth_status) {
        this.user_auth_status = user_auth_status;
    }

    public List<AuthInfoBean> getAuth_info() {
        return auth_info;
    }

    public void setAuth_info(List<AuthInfoBean> auth_info) {
        this.auth_info = auth_info;
    }

    public static class AuthInfoBean {
        /**
         * addtime : 2021-12-07 10:05:36
         * auth_icon : http://live1v1test2.yunbaozhibo.com/static/app/auth/user_auth.png
         * auth_title : 实名认证
         * auth_type : user_auth
         * cardno : 370911199105087230
         * mobile : 15588527855
         * name : 车德东
         * reason :
         * status : 1
         * uid : 103058
         * uptime : 2021-12-09 14:27:37
         * backwall : qiniu_android_103058_20211209_172527_8954863.png
         * backwall_list : ["qiniu_android_103058_20211209_172527_8954863.png"]
         * backwall_list_format : ["http://yb1v1.yunbaozb.com/android_103058_20211209_172527_8954863.png"]
         * id : 6
         * thumb : qiniu_android_103058_20211209_172527_6771862.png
         * thumb_format : http://yb1v1.yunbaozb.com/android_103058_20211209_172527_6771862.png
         * video : qiniu_android_103058_20211209_172528_0436775.mp4
         * video_format : http://yb1v1.yunbaozb.com/android_103058_20211209_172528_0436775.mp4
         * video_thumb :
         * video_thumb_format :
         */

        private String addtime;
        private String auth_icon;
        private String auth_title;
        private String auth_type;
        private String cardno;
        private String mobile;
        private String name;
        private String reason;
        private String status;
        private String uid;
        private String uptime;
        private String backwall;
        private String id;
        private String thumb;
        private String thumb_format;
        private String video;
        private String video_format;
        private String video_thumb;
        private String video_thumb_format;
        private List<String> backwall_list;
        private List<String> backwall_list_format;

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public String getAuth_icon() {
            return auth_icon;
        }

        public void setAuth_icon(String auth_icon) {
            this.auth_icon = auth_icon;
        }

        public String getAuth_title() {
            return auth_title;
        }

        public void setAuth_title(String auth_title) {
            this.auth_title = auth_title;
        }

        public String getAuth_type() {
            return auth_type;
        }

        public void setAuth_type(String auth_type) {
            this.auth_type = auth_type;
        }

        public String getCardno() {
            return cardno;
        }

        public void setCardno(String cardno) {
            this.cardno = cardno;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getUptime() {
            return uptime;
        }

        public void setUptime(String uptime) {
            this.uptime = uptime;
        }

        public String getBackwall() {
            return backwall;
        }

        public void setBackwall(String backwall) {
            this.backwall = backwall;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getThumb_format() {
            return thumb_format;
        }

        public void setThumb_format(String thumb_format) {
            this.thumb_format = thumb_format;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getVideo_format() {
            return video_format;
        }

        public void setVideo_format(String video_format) {
            this.video_format = video_format;
        }

        public String getVideo_thumb() {
            return video_thumb;
        }

        public void setVideo_thumb(String video_thumb) {
            this.video_thumb = video_thumb;
        }

        public String getVideo_thumb_format() {
            return video_thumb_format;
        }

        public void setVideo_thumb_format(String video_thumb_format) {
            this.video_thumb_format = video_thumb_format;
        }

        public List<String> getBackwall_list() {
            return backwall_list;
        }

        public void setBackwall_list(List<String> backwall_list) {
            this.backwall_list = backwall_list;
        }

        public List<String> getBackwall_list_format() {
            return backwall_list_format;
        }

        public void setBackwall_list_format(List<String> backwall_list_format) {
            this.backwall_list_format = backwall_list_format;
        }
    }
}
