package com.yunbao.main.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yunbao.common.event.HomeHeadClickEvent;
import com.yunbao.common.event.VideoEvent;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.ScreenDimenUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.views.AbsViewHolder;
import com.yunbao.main.R;
import com.yunbao.main.bean.WallBean;

import org.greenrobot.eventbus.EventBus;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class UserHomeWallViewHolder extends AbsViewHolder implements UserHomeWallVideoViewHolder.ActionListener {

    private ImageView mCover;
    private boolean mFristLoad;
    private UserHomeWallVideoViewHolder mVideoVh;
    private WallBean mWallBean;

    public UserHomeWallViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_user_home_wall;
    }

    @Override
    public void init() {
        mFristLoad = true;
        mCover =findViewById(R.id.img);
    }


    public void loadData(WallBean wallBean) {
        if (!mFristLoad || wallBean == null) {
            return;
        }
        mWallBean = wallBean;
        mFristLoad = false;
        if (wallBean.isVideo()) {
            findViewById(R.id.root).setBackgroundColor(0xff000000);
            ImgLoader.displayDrawable(mContext, wallBean.getThumb(), new ImgLoader.DrawableCallback() {
                @Override
                public void onLoadSuccess(Drawable drawable) {
                    if (mCover != null && mCover.getVisibility() == View.VISIBLE && drawable != null) {
                        float w = drawable.getIntrinsicWidth();
                        float h = drawable.getIntrinsicHeight();
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mCover.getLayoutParams();
                        int targetH = 0;
                        if (w / h > 0.5625f) {//横屏  9:16=0.5625
                            targetH = (int) (ScreenDimenUtil.getInstance().getScreenWdith() / w * h);
                        } else {
                            targetH = ViewGroup.LayoutParams.MATCH_PARENT;
                        }
                        if (targetH != params.height) {
                            params.height = targetH;
                            params.gravity = Gravity.CENTER;
                            mCover.requestLayout();
                        }
                        mCover.setImageDrawable(drawable);
                    }
                }

                @Override
                public void onLoadFailed() {

                }
            });
            UserHomeWallVideoViewHolder vh = new UserHomeWallVideoViewHolder(mContext, (ViewGroup) findViewById(R.id.video_container));
            mVideoVh = vh;
            vh.addToParent();
            vh.subscribeActivityLifeCycle();
            vh.setActionListener(this);
            vh.startPlay(wallBean.getHref());
            EventBus.getDefault().post(new VideoEvent(vh.isPlaying()));

        } else {
            ImgLoader.display(mContext, wallBean.getThumb(), mCover);
        }

        mCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new HomeHeadClickEvent());
            }
        });
    }


    /**
     * 被动暂停播放(请不要问我为啥要这么改！！（两端统一）)
     */
    public void passivePause() {
        if (mVideoVh != null) {
//            mVideoVh.passivePause();
            mVideoVh.stopPlay();
        }
    }


    /**
     * 静音
     */
    public void passiveMute(boolean mute) {
        if (mVideoVh != null && mWallBean != null) {
            mVideoVh.setMute(mute);
        }
    }

    /**
     * 被动恢复播放
     */
    public void passiveResume() {
        if (mVideoVh != null && mWallBean != null) {
//            mVideoVh.passiveResume();
            mVideoVh.startPlay(mWallBean.getHref());
        }
    }



    @Override
    public void onFirstFrame() {
        if (mCover != null && mCover.getVisibility() == View.VISIBLE) {
            mCover.setVisibility(View.INVISIBLE);
        }
    }
}
