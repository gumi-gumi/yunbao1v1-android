package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.main.R;
import com.yunbao.main.R2;
import com.yunbao.main.http.MainHttpUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RealNameCheckActivity extends AbsActivity  {

    @BindView(R2.id.titleView)
    TextView titleView;
    @BindView(R2.id.btn_back)
    ImageView btnBack;

    private EditText etName,etCd,etPhone;
    private String realName,cardNo,phoneNumber,status;
    private Button btnSubmit;
    public static void forward(Context context,String name,String cardNo,String phone,String status) {
        Intent intent = new Intent(context, RealNameCheckActivity.class);
        intent.putExtra("name",name);
        intent.putExtra("cardNo",cardNo);
        intent.putExtra("phone",phone);
        intent.putExtra("status",status);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_real_name_check;
    }

    @Override
    protected void main() {
        Intent intent=getIntent();
        realName=intent.getStringExtra("name");
        cardNo=intent.getStringExtra("cardNo");
        phoneNumber=intent.getStringExtra("phone");
        status=intent.getStringExtra("status");

        etName=findViewById(R.id.et_name);
        etCd=findViewById(R.id.et_cd);
        etPhone=findViewById(R.id.et_phone);
        btnSubmit=findViewById(R.id.btn_submit);

        etName.setText(realName);
        etCd.setText(cardNo);
        etPhone.setText(phoneNumber);

        if ("0".equals(status)){
            btnSubmit.setText(R.string.video_status_verify);
            btnSubmit.setBackgroundResource(R.drawable.bg_main_me_false);
            btnSubmit.setClickable(false);

        }else{
            btnSubmit.setText(R.string.check_fail);
            btnSubmit.setBackgroundResource(R.drawable.bg_main_me_charge);
            btnSubmit.setClickable(true);
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etName.getText())){
                    ToastUtil.show(R.string.name_no);
                }else if (TextUtils.isEmpty(etCd.getText())){
                    ToastUtil.show(R.string.card_no);
                }else if (TextUtils.isEmpty(etPhone.getText())){
                    ToastUtil.show(R.string.mobile_no);
                }else {
                    submit();//提交认证
                }

            }
        });

    }

    /**
     * 实名认证
     */
    private void submit(){
        MainHttpUtil.setUserAuth(etName.getText().toString(), etPhone.getText().toString(), etCd.getText().toString(), new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if(code==0){
                    ToastUtil.show(msg);
                    finish();
                }else{
                    ToastUtil.show(msg);
                }

            }
        });
    }

}
