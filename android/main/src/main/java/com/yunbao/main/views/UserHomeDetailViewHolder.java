package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.GiftCabActivity;
import com.yunbao.main.activity.UserHomeNewActivity;
import com.yunbao.main.adapter.GiftCabAdapter;
import com.yunbao.main.adapter.UserHomeDetailAdapter;
import com.yunbao.main.bean.GiftCabBean;
import com.yunbao.one.http.OneHttpConsts;
import com.yunbao.one.http.OneHttpUtil;
import com.yunbao.one.views.AbsUserHomeViewHolder;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class UserHomeDetailViewHolder extends AbsUserHomeViewHolder implements View.OnClickListener {

    private CommonRefreshView mRefreshView;
    private UserHomeDetailAdapter mUserHomeDetailAdapter;
    private String mToUid;
    private TextView mOnLine;
    private TextView mLintenRate;
    private TextView mHeight;
    private TextView mWeight;
    private TextView mStar;
    private TextView mGiftCount;
    private RecyclerView mGiftRecyclerView;
    private View mNoGift;
    private SpannableStringBuilder mBuilder;
    private ForegroundColorSpan mColorSpan;


    public UserHomeDetailViewHolder(Context context, ViewGroup parentView, String toUid) {
        super(context, parentView, toUid);
    }

    @Override
    protected void processArguments(Object... args) {
        mToUid = (String) args[0];
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_user_home_detail;
    }

    @Override
    public void init() {
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        mRefreshView.setEmptyLayoutId(0);
        mRefreshView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mUserHomeDetailAdapter = new UserHomeDetailAdapter(mContext);
        View headView = mUserHomeDetailAdapter.getHeadView();
        mRefreshView.setRecyclerViewAdapter(mUserHomeDetailAdapter);
        mOnLine = headView.findViewById(R.id.on_line);
        mLintenRate = headView.findViewById(R.id.listen_rate);
        mHeight = headView.findViewById(R.id.height);
        mWeight = headView.findViewById(R.id.weight);
        mStar = headView.findViewById(R.id.star);
        mGiftCount = headView.findViewById(R.id.gift_count);
        mGiftRecyclerView = headView.findViewById(R.id.gift_recyclerView);
        mGiftRecyclerView.setHasFixedSize(true);
        mGiftRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 5, GridLayoutManager.VERTICAL, false));
        mNoGift = headView.findViewById(R.id.no_gift);
        headView.findViewById(R.id.btn_gift_cab).setOnClickListener(this);

    }

    @Override
    public void loadData() {
        JSONObject obj = ((UserHomeNewActivity) mContext).getUserObj();
        if (obj == null) {
            return;
        }
        if (!isFirstLoadData()) {
            return;
        }

        if (mOnLine != null) {
            mOnLine.setText(createColorString(WordUtil.getString(R.string.user_home_online), obj.getString("last_online_time")));
        }
        if (mLintenRate != null) {
            mLintenRate.setText(createColorString(WordUtil.getString(R.string.user_home_listen_rate), obj.getString("answer_rate")));
        }
        if (mHeight != null) {
            mHeight.setText(createColorString(WordUtil.getString(R.string.user_home_height), obj.getString("height")));
        }
        if (mWeight != null) {
            mWeight.setText(createColorString(WordUtil.getString(R.string.user_home_weight), obj.getString("weight")));
        }

        if (mStar != null) {
            mStar.setText(createColorString(WordUtil.getString(R.string.user_home_star), obj.getString("constellation")));
        }
        if (mGiftCount != null) {
            mGiftCount.setText(obj.getString("gift_total"));
        }

        List<GiftCabBean> giftList = JSON.parseArray(obj.getString("gift_list"), GiftCabBean.class);
        if (giftList != null && giftList.size() > 0) {
            if (mNoGift != null) {
                mNoGift.setVisibility(View.INVISIBLE);
            }
            if (mGiftRecyclerView != null) {
                mGiftRecyclerView.setVisibility(View.VISIBLE);
                GiftCabAdapter adapter = new GiftCabAdapter(mContext, giftList);
                mGiftRecyclerView.setAdapter(adapter);
            }
        }

    }


    private SpannableStringBuilder createColorString(String text1, String text2) {
        if (mBuilder == null) {
            mBuilder = new SpannableStringBuilder();
        } else {
            mBuilder.delete(0, mBuilder.length());
        }
        if (mColorSpan == null) {
            mColorSpan = new ForegroundColorSpan(0xff323232);
        }
        mBuilder.append(text1);
        mBuilder.append(text2);
        mBuilder.setSpan(mColorSpan, text1.length(), mBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return mBuilder;
    }


    @Override
    public void onClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.btn_gift_cab) {
            GiftCabActivity.forward(mContext, mToUid);
        }

    }
}
