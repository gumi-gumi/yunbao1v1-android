package com.yunbao.main.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;

import com.alibaba.fastjson.JSON;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.custom.ItemDecoration;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.adapter.MyWallAdapter;
import com.yunbao.main.bean.WallBean;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 背景墙
 */
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MyWallActivity extends AbsActivity implements MyWallAdapter.ActionListener {

    public static void forward(Context context) {
        context.startActivity(new Intent(context, MyWallActivity.class));
    }

    private CommonRefreshView mRefreshView;
    private MyWallAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_wall;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.wall));
        mRefreshView = findViewById(R.id.refreshView);
        mRefreshView.setLayoutManager(new GridLayoutManager(mContext, 3, GridLayoutManager.VERTICAL, false));
        ItemDecoration decoration = new ItemDecoration(mContext, 0x00000000, 2, 0);
        decoration.setOnlySetItemOffsetsButNoDraw(true);
        mRefreshView.setItemDecoration(decoration);
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<WallBean>() {
            @Override
            public RefreshAdapter<WallBean> getAdapter() {
                if (mAdapter == null) {
                    mAdapter = new MyWallAdapter(mContext);
                    mAdapter.setActionListener(MyWallActivity.this);
                }
                return mAdapter;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                MainHttpUtil.getMyWall(callback);
            }

            @Override
            public List<WallBean> processData(String[] info) {
                List<WallBean> list = JSON.parseArray(Arrays.toString(info), WallBean.class);
                if (list.size() < Constants.AUTH_IMAGE_MAX_SIZE) {
                    WallBean bean = new WallBean();
                    bean.setAdd(true);
                    list.add(bean);
                }
                return list;
            }

            @Override
            public void onRefreshSuccess(List<WallBean> list, int listCount) {

            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<WallBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRefreshView != null) {
            mRefreshView.initData();
        }
    }


    @Override
    protected void onDestroy() {
        MainHttpUtil.cancel(MainHttpConsts.GET_MY_WALL);
        super.onDestroy();
    }


    @Override
    public void onItemClick(WallBean bean) {
        if (mAdapter == null) {
            return;
        }
        MyWallDetailActivity.forward(mContext, bean, mAdapter.getImageCount(), bean.isVideo());
    }

    @Override
    public void onAddClick() {
        if(!ClickUtil.canClick()){
            return;
        }
        DialogUitl.showStringArrayDialog(mContext, new Integer[]{R.string.wall_add_img}, true, new DialogUitl.StringArrayDialogCallback() {
            @Override
            public void onItemClick(String text, int tag) {
                if (tag == R.string.wall_add_img) {
                    MyWallChooseImageActivity.forward(mContext, 0, null);
                }
            }
        });
    }


}
