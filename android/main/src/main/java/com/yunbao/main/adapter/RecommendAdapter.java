package com.yunbao.main.adapter;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yunbao.common.custom.MyRadioButton;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.CommonIconUtil;
import com.yunbao.main.R;
import com.yunbao.main.bean.RecommendUserBean;

import java.util.ArrayList;
import java.util.List;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RecommendAdapter extends BaseQuickAdapter<RecommendUserBean, BaseViewHolder> {
    public RecommendAdapter() {
        super(R.layout.item_recommend);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, RecommendUserBean bean) {
        ImgLoader.display(mContext, bean.getAvatar(), (ImageView) helper.getView(R.id.avatar));
        MyRadioButton mCheckButton = helper.getView(R.id.radioButton);
        mCheckButton.doChecked(bean.isCheck());
        helper.setText(R.id.name, bean.getUser_nickname());
        ImageView mIvSex = helper.getView(R.id.sex);
        mIvSex.setImageResource(CommonIconUtil.getSexIcon(Integer.parseInt(bean.getSex())));
    }

    public void updateData(int pos) {
        mData.get(pos).setCheck(!mData.get(pos).isCheck());
        refreshNotifyItemChanged(pos);
    }

    public List<String> getAllCheckedUids() {
        List<String> mUids = new ArrayList<>();
        mUids.clear();
        for (RecommendUserBean bean : mData) {
            if (bean.isCheck()) {
                mUids.add(bean.getId());
            }
        }
        return mUids;
    }
}
