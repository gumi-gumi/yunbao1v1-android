package com.yunbao.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.ViewGroup;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.main.R;
import com.yunbao.main.views.MainFindMatchViewHolder;

import org.json.JSONException;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MatchActivity extends AbsActivity {
    private MainFindMatchViewHolder mMainFindMatchViewHolder;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_match;
    }

    @Override
    protected void main() {
        super.main();
        mMainFindMatchViewHolder = new MainFindMatchViewHolder(mContext, (ViewGroup) findViewById(R.id.container));
        mMainFindMatchViewHolder.addToParent();
        mMainFindMatchViewHolder.subscribeActivityLifeCycle();
        mMainFindMatchViewHolder.loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMainFindMatchViewHolder != null) {
            mMainFindMatchViewHolder.release();
        }
        mMainFindMatchViewHolder = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

        }
    }
}
