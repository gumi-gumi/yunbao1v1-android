package com.yunbao.main.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.bean.ChatPriceBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.bean.UserItemBean;
import com.yunbao.common.dialog.MainPriceDialogFragment;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.RouteUtil;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.AuthActivity;
import com.yunbao.main.activity.EditProfileActivity;
import com.yunbao.main.activity.FansActivity;
import com.yunbao.main.activity.FollowActivity;
import com.yunbao.main.activity.GiftCabActivity;
import com.yunbao.main.activity.MyPhotoActivity;
import com.yunbao.main.activity.MyProfitActivity;
import com.yunbao.main.activity.MyWallActivity;
import com.yunbao.main.activity.SettingActivity;
import com.yunbao.main.activity.WalletActivity;
import com.yunbao.main.adapter.MainMeAdapter;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import java.util.List;

/**
 * 我的
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainMeViewHolder extends AbsMainViewHolder implements View.OnClickListener, MainMeAdapter.ActionListener, MainPriceDialogFragment.ActionListener {

    private RecyclerView mRecyclerView;
    private MainMeAdapter mAdapter;
    private ImageView mAvatar;
    private TextView mName;
    private TextView mID;
    private TextView mFollow;
    private TextView mFans;
    private TextView mCoin;
    private TextView mProfit;
    private boolean mPaused;
    private boolean mIsAnchorAuth;


    public MainMeViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_me;
    }

    @Override
    public void init() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mAdapter = new MainMeAdapter(mContext);
        mAdapter.setActionListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mAvatar = findViewById(R.id.avatar);
        mName = findViewById(R.id.name);
        mID = findViewById(R.id.id_val);
        mFollow = findViewById(R.id.follow);
        mFans = findViewById(R.id.fans);
        mCoin = findViewById(R.id.coin);
        mProfit = findViewById(R.id.profit);
        findViewById(R.id.btn_edit).setOnClickListener(this);
        findViewById(R.id.btn_follow).setOnClickListener(this);
        findViewById(R.id.btn_charge).setOnClickListener(this);
        findViewById(R.id.btn_setting).setOnClickListener(this);
        findViewById(R.id.btn_profit).setOnClickListener(this);
        findViewById(R.id.btn_fans).setOnClickListener(this);
        findViewById(R.id.btn_auth).setOnClickListener(this);
        findViewById(R.id.btn_wallet).setOnClickListener(this);
        CommonAppConfig appConfig = CommonAppConfig.getInstance();
        UserBean u = appConfig.getUserBean();
        List<UserItemBean> list = appConfig.getUserItemList();
        if (u != null) {
            mIsAnchorAuth = u.getIsAuthorAuth() == 1;
            showData(u, null, list);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isShowed() && mPaused) {
            loadData();
        }
        mPaused = false;
    }


    @Override
    public void loadData() {
        MainHttpUtil.getBaseInfo(mCallback);
    }

    private CommonCallback<Object[]> mCallback = new CommonCallback<Object[]>() {
        @Override
        public void callback(Object[] bean) {
            List<UserItemBean> list = CommonAppConfig.getInstance().getUserItemList();
            if (bean != null) {
                showData((UserBean) bean[0], (JSONObject) bean[1], list);
            }
        }
    };

    private void showData(UserBean u, JSONObject obj, List<UserItemBean> list) {
        ImgLoader.displayAvatar(mContext, u.getAvatarThumb(), mAvatar);
        mName.setText(u.getUserNiceName());
        mID.setText(StringUtil.contact("ID:", u.getId()));
        mFollow.setText(StringUtil.toWan(u.getFollows()));
        mFans.setText(StringUtil.toWan(u.getFans()));
        mCoin.setText(u.getCoin());
        mProfit.setText(u.getVotesTotal());
        if (obj != null) {
            mIsAnchorAuth = obj.getIntValue("isauth") == 1;
        }
        if (mAdapter != null && list != null && list.size() > 0) {
            mAdapter.setList(list);
        }


    }


    @Override
    public void onItemClick(UserItemBean bean) {
        if (!ClickUtil.canClick()) {
            return;
        }
        switch (bean.getId()) {
            case Constants.MAIN_ME_VIDEO:
                videoPriceClick();
                break;
            case Constants.MAIN_ME_GIFI_CAB:
                GiftCabActivity.forward(mContext, CommonAppConfig.getInstance().getUid());
                break;
            case Constants.MAIN_ME_MY_ALBUM:
                forwardMyAlbum();
                break;
            case Constants.MAIN_ME_WALL:
                MyWallActivity.forward(mContext);
                break;

        }
    }

    @Override
    public void onRadioBtnChanged(UserItemBean bean) {
        switch (bean.getId()) {
            case Constants.MAIN_ME_VIDEO:
                setVideoSwitch(bean.isRadioBtnChecked());
                break;
        }
    }


    @Override
    public void onClick(View v) {
        if (!ClickUtil.canClick()) {
            return;
        }
        int i = v.getId();
        if (i == R.id.btn_edit) {
            forwardEditProfile();
        } else if (i == R.id.btn_follow) {
            forwardFollow();
        } else if (i == R.id.btn_fans) {
            forwardFans();
        } else if (i == R.id.btn_charge) {
            RouteUtil.forwardMyCoin();
        } else if (i == R.id.btn_setting) {
            forwardSetting();
        } else if (i == R.id.btn_profit) {
            MyProfitActivity.forward(mContext);
        } else if (i == R.id.btn_auth) {
            forwardAuth();
        } else if (i == R.id.btn_wallet) {
            forwardWallet();
        }

    }

    /**
     * 编辑个人资料
     */
    private void forwardEditProfile() {
        UserBean u = CommonAppConfig.getInstance().getUserBean();
        if (u.getSex() == 0) {
            EditProfileActivity.forward(mContext, 2);
        } else {
            EditProfileActivity.forward(mContext, u.getSex());
        }
    }

    /**
     * 我的关注
     */
    private void forwardFollow() {
        FollowActivity.forward(mContext, CommonAppConfig.getInstance().getUid());
    }

    /**
     * 我的粉丝
     */
    private void forwardFans() {
        FansActivity.forward(mContext, CommonAppConfig.getInstance().getUid());
    }


    /**
     * 我要认证
     */
    private void forwardAuth() {
        AuthActivity.forward(mContext);

    }

    /**
     * 我的钱包
     */
    private void forwardWallet() {
        mContext.startActivity(new Intent(mContext, WalletActivity.class));
    }


    /**
     * 设置
     */
    private void forwardSetting() {
        mContext.startActivity(new Intent(mContext, SettingActivity.class));
    }


    /**
     * 我的视频
     */
    private void forwardMyVideo() {
    }

    /**
     * 我的相册
     */
    private void forwardMyAlbum() {
        mContext.startActivity(new Intent(mContext, MyPhotoActivity.class));
    }


    /**
     * 设置视频接听开关
     */
    private void setVideoSwitch(final boolean open) {
        MainHttpUtil.setVideoSwitch(open, new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0) {
                    CommonAppConfig.getInstance().setUserSwitchVideo(open);
                }
                ToastUtil.show(msg);
            }
        });
    }


    /**
     * 设置视频价格
     */
    private void videoPriceClick() {
        MainPriceDialogFragment fragment = new MainPriceDialogFragment();
        CommonAppConfig appConfig = CommonAppConfig.getInstance();
        fragment.setPriceList(appConfig.getVideoPriceList());
        fragment.setNowPrice(appConfig.getPriceVideo());
        fragment.setFrom(Constants.MAIN_ME_VIDEO);
        fragment.setActionListener(this);
        fragment.show(((AbsActivity) mContext).getSupportFragmentManager(), "MainPriceDialogFragment");
    }


    @Override
    public void onPriceSelected(int from, ChatPriceBean bean) {
        if (from == Constants.MAIN_ME_VIDEO) {
            setVideoPrice(bean);
        }
    }

    /**
     * 设置视频价格
     */
    private void setVideoPrice(final ChatPriceBean bean) {
        MainHttpUtil.setVideoPrice(bean.getCoin(), new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0) {
                    CommonAppConfig.getInstance().setPriceVideo(bean.getCoin());
                    if (mAdapter != null) {
                        mAdapter.updateVideoPrice(bean.getCoin());
                    }
                }
                ToastUtil.show(msg);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        MainHttpUtil.cancel(MainHttpConsts.GET_BASE_INFO);
        MainHttpUtil.cancel(MainHttpConsts.SET_DISTURB_SWITCH);
        MainHttpUtil.cancel(MainHttpConsts.SET_VIDEO_SWITCH);
        MainHttpUtil.cancel(MainHttpConsts.SET_VIDEO_PRICE);
    }
}
