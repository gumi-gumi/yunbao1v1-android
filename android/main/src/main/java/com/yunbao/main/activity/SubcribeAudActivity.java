package com.yunbao.main.activity;

import android.view.ViewGroup;

import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.views.SubcribeAudViewHolder;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class SubcribeAudActivity extends AbsActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_subcribe_aud;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.chat_subcribe));
        SubcribeAudViewHolder viewHolder = new SubcribeAudViewHolder(mContext, (ViewGroup) findViewById(R.id.container));
        viewHolder.addToParent();
        viewHolder.subscribeActivityLifeCycle();
        viewHolder.loadData();
    }
}
