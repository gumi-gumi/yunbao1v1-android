package com.yunbao.main.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.bean.ChatPriceBean;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.ActivityResultCallback;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.interfaces.ImageResultCallback;
import com.yunbao.common.upload.FileUploadManager;
import com.yunbao.common.upload.UploadBean;
import com.yunbao.common.upload.UploadCallback;
import com.yunbao.common.upload.UploadStrategy;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.L;
import com.yunbao.common.utils.ProcessImageUtil;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.im.activity.ChooseImageActivity;
import com.yunbao.main.R;
import com.yunbao.main.custom.UploadImageView2;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * 上传相册图片
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MyPhotoPubActivity extends AbsActivity implements View.OnClickListener {

    private static final String TAG = "MyAlbumPubActivity";

    public static void forward(Context context) {
        context.startActivity(new Intent(context, MyPhotoPubActivity.class));
    }

    private Dialog mChooseImageDialog;
    private ActivityResultCallback mChooseImageCallback;
    private ProcessImageUtil mImageUtil;
    private UploadImageView2 mImgUpload;
    private UploadBean mUploadBean;
    private View mBtnPub;
    private List<ChatPriceBean> mPriceList;
    private UploadStrategy mUploadStrategy;
    private Dialog mLoading;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_album_pub;
    }

    @Override
    protected void main() {
        setTitle(WordUtil.getString(R.string.album_pub));
        mImageUtil = new ProcessImageUtil(this);
        mImageUtil.setImageResultCallback(new ImageResultCallback() {
            @Override
            public void beforeCamera() {

            }

            @Override
            public void onSuccess(File file) {
                if (file == null) {
                    return;
                }
                if (mImgUpload != null && mUploadBean != null) {
                    mUploadBean.setOriginFile(file);
                    mImgUpload.showImageData(mUploadBean);
                }
            }

            @Override
            public void onFailure() {
            }
        });
        mUploadBean = new UploadBean();
        mImgUpload = findViewById(R.id.img_upload);
        mImgUpload.setActionListener(new UploadImageView2.ActionListener() {
            @Override
            public void onAddClick(UploadImageView2 uploadImageView) {
                chooseImage();
            }
        });
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        mBtnPub = findViewById(R.id.btn_upload);
        mBtnPub.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_cancel) {
            cancel();
        } else if (i == R.id.btn_upload) {
            uploadImage();
        }
    }


    private void cancel() {
        if (mUploadBean == null || mUploadBean.isEmpty()) {
            MyPhotoPubActivity.super.onBackPressed();
        } else {
            DialogUitl.showStringArrayDialog(
                    mContext, new Integer[]{R.string.upload_give_up}, new DialogUitl.StringArrayDialogCallback() {
                        @Override
                        public void onItemClick(String text, int tag) {
                            MyPhotoPubActivity.super.onBackPressed();
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        cancel();
    }

    /**
     * 选择图片
     */
    public void chooseImage() {
        if (mChooseImageDialog == null) {
            mChooseImageDialog = DialogUitl.getStringArrayDialog(mContext, new Integer[]{
                    R.string.camera, R.string.alumb}, true, new DialogUitl.StringArrayDialogCallback() {
                @Override
                public void onItemClick(String text, int tag) {
                    if (tag == R.string.camera) {
                        mImageUtil.getImageByCamera(false);
                    } else {
                        mImageUtil.getImageByAlumb(false);
                    }
                }
            });
        }
        mChooseImageDialog.show();
    }

    /**
     * 选择图片，检查读写权限
     */
    private void checkReadWritePermissions() {
        if (mImageUtil == null) {
            return;
        }
        mImageUtil.requestPermissions(
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                new CommonCallback<Boolean>() {
                    @Override
                    public void callback(Boolean result) {
                        if (result) {
                            forwardChooseImage();
                        }
                    }
                });
    }

    /**
     * 跳转到选择图片页面
     */
    private void forwardChooseImage() {
        if (mChooseImageCallback == null) {
            mChooseImageCallback = new ActivityResultCallback() {
                @Override
                public void onSuccess(Intent intent) {
                    if (intent != null) {
                        String imagePath = intent.getStringExtra(Constants.SELECT_IMAGE_PATH);
                        if (!TextUtils.isEmpty(imagePath)) {
                            File file = new File(imagePath);
                            if (file.exists()) {
                                if (mImgUpload != null && mUploadBean != null) {
                                    mUploadBean.setOriginFile(file);
                                    mImgUpload.showImageData(mUploadBean);
                                }
                            }
                        }
                    }
                }
            };
        }
        mImageUtil.startActivityForResult(new Intent(mContext, ChooseImageActivity.class), mChooseImageCallback);
    }


    /**
     * 上传图片
     */
    private void uploadImage() {
        if (mUploadBean == null) {
            ToastUtil.show(R.string.album_upload_empty);
            return;
        }
        File file = mUploadBean.getOriginFile();
        if (file == null || !file.exists() || file.length() == 0) {
            ToastUtil.show(R.string.album_upload_empty);
            return;
        }
        if (mLoading == null) {
            mLoading = DialogUitl.loadingDialog(mContext);
        }
        mLoading.show();
        FileUploadManager.getInstance().createUploadImpl(mContext, new CommonCallback<UploadStrategy>() {
            @Override
            public void callback(UploadStrategy strategy) {
                if (strategy == null) {
                    ToastUtil.show(WordUtil.getString(R.string.upload_type_error));
                    return;
                }
                mUploadStrategy = strategy;
                mUploadStrategy.upload(Arrays.asList(mUploadBean), true, new UploadCallback() {
                    @Override
                    public void onFinish(List<UploadBean> list, boolean success) {
                        if (success) {
                            L.e(TAG, "上传图片完成---------> " + success);
                            if (list != null && list.size() > 0) {
                                upload(list.get(0).getRemoteFileName());
                            }
                        }
                    }
                });
            }
        });
    }

    /**
     * 把相册图片信息保存在服务器
     */
    private void upload(String thumb) {
        MainHttpUtil.setPhoto(thumb,  0,"0", new HttpCallback() {
            @Override
            public void onSuccess(int code, String msg, String[] info) {
                if (code == 0) {
                    if (mImgUpload != null && mUploadBean != null) {
                        mUploadBean.setEmpty();
                        mImgUpload.showImageData(mUploadBean);
                    }
                }
                ToastUtil.show(msg);
            }

            @Override
            public void onFinish() {
                if (mLoading != null) {
                    mLoading.dismiss();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        MainHttpUtil.cancel(MainHttpConsts.SET_PHOTO);
        if (mImageUtil != null) {
            mImageUtil.release();
        }
        mImageUtil = null;
        super.onDestroy();
    }
}
