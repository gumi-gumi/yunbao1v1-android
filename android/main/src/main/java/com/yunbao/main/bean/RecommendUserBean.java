package com.yunbao.main.bean;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class RecommendUserBean {
    /**
     * id : 102955
     * user_nickname : bad guy
     * avatar : http://thirdqq.qlogo.cn/g?b=oidb&k=icYvV4v2yyfZbmqzn7l93pg&s=100&t=1609512573
     * avatar_thumb : http://thirdqq.qlogo.cn/g?b=oidb&k=icYvV4v2yyfZbmqzn7l93pg&s=100&t=1609512573
     * sex : 1
     * isauth : 1
     */

    private String id;
    private String user_nickname;
    private String avatar;
    private String avatar_thumb;
    private String sex;
    private String isauth;
    private boolean isCheck = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar_thumb() {
        return avatar_thumb;
    }

    public void setAvatar_thumb(String avatar_thumb) {
        this.avatar_thumb = avatar_thumb;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIsauth() {
        return isauth;
    }

    public void setIsauth(String isauth) {
        this.isauth = isauth;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
