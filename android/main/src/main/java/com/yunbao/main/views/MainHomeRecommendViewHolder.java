package com.yunbao.main.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.activity.WebViewActivity;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.ChargeSuccessBean;
import com.yunbao.common.custom.CommonRefreshView;
import com.yunbao.common.custom.ItemDecoration;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.interfaces.OnItemClickListener;
import com.yunbao.main.R;
import com.yunbao.main.activity.RankListActivity;
import com.yunbao.main.adapter.MainHomeRecommendAdapter;
import com.yunbao.main.bean.BannerBean;
import com.yunbao.main.dialog.MatchDialogFragment;
import com.yunbao.main.http.MainHttpConsts;
import com.yunbao.main.http.MainHttpUtil;
import com.yunbao.main.presenter.ChargeAnimPresenter;
import com.yunbao.one.bean.ChatLiveBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * MainActivity 首页 推荐
 */

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainHomeRecommendViewHolder extends AbsMainHomeChildViewHolder implements OnItemClickListener<ChatLiveBean>,  View.OnClickListener {

    private CommonRefreshView mRefreshView;
    private Banner mBanner;
    private boolean mBannerShowed;
    private MainHomeRecommendAdapter mAdapter;
    private List<BannerBean> mBannerList;
    private byte mSex;
    private byte mChatType;
    private ChargeAnimPresenter mChargeAnimPresenter;
    private boolean mPaused;



    public MainHomeRecommendViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
        mSex = Constants.MAIN_SEX_NONE;
        mChatType = Constants.CHAT_TYPE_NONE;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_home_recommend;
    }

    @Override
    public void init() {
        mRefreshView = (CommonRefreshView) findViewById(R.id.refreshView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 2;
                }
                return 1;
            }
        });
        mRefreshView.setLayoutManager(gridLayoutManager);
        ItemDecoration decoration = new ItemDecoration(mContext, 0x00000000, 5, 0);
        decoration.setOnlySetItemOffsetsButNoDraw(true);
        mRefreshView.setItemDecoration(decoration);
        mAdapter = new MainHomeRecommendAdapter(mContext);
        mAdapter.setOnItemClickListener(MainHomeRecommendViewHolder.this);
        mRefreshView.setRecyclerViewAdapter(mAdapter);
        View headView = mAdapter.getHeadView();
        if (headView != null) {
            mBanner = (Banner) headView.findViewById(R.id.banner);
            mBanner.setImageLoader(new ImageLoader() {
                @Override
                public void displayImage(Context context, Object path, ImageView imageView) {
                    ImgLoader.display(mContext, ((BannerBean) path).getImageUrl(), imageView);
                }
            });
            mBanner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int p) {
                    if (mBannerList != null) {
                        if (p >= 0 && p < mBannerList.size()) {
                            BannerBean bean = mBannerList.get(p);
                            if (bean != null) {
                                String link = bean.getLink();
                                if (!TextUtils.isEmpty(link)) {
                                    WebViewActivity.forward(mContext, link, false);
                                }
                            }
                        }
                    }
                }
            });

            headView.findViewById(R.id.btn_match_voice).setOnClickListener(this);
            headView.findViewById(R.id.btn_rank).setOnClickListener(this);
        }
        mRefreshView.setDataHelper(new CommonRefreshView.DataHelper<ChatLiveBean>() {
            @Override
            public RefreshAdapter<ChatLiveBean> getAdapter() {
                return null;
            }

            @Override
            public void loadData(int p, HttpCallback callback) {
                MainHttpUtil.getHot(p, mSex, mChatType, callback);
            }

            @Override
            public List<ChatLiveBean> processData(String[] info) {
                JSONObject obj = JSON.parseObject(info[0]);
                mBannerList = JSON.parseArray(obj.getString("slide"), BannerBean.class);
                return JSON.parseArray(JSON.parseObject(info[0]).getString("list"), ChatLiveBean.class);
            }

            @Override
            public void onRefreshSuccess(List<ChatLiveBean> list, int count) {
                showBanner();
            }

            @Override
            public void onRefreshFailure() {

            }

            @Override
            public void onLoadMoreSuccess(List<ChatLiveBean> loadItemList, int loadItemCount) {

            }

            @Override
            public void onLoadMoreFailure() {

            }
        });
        EventBus.getDefault().register(this);
    }

    private void showBanner() {
        if (mBannerList == null || mBannerList.size() == 0 || mBanner == null) {
            return;
        }
        if (mBannerShowed) {
            return;
        }
        mBannerShowed = true;
        mBanner.setImages(mBannerList);
        mBanner.start();
    }


    @Override
    public void onItemClick(ChatLiveBean bean, int position) {
        forwardUserHome(bean.getUid());
    }

    @Override
    public void loadData() {
        if (isFirstLoadData() && mRefreshView != null) {
            mRefreshView.initData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChargeSuccessBean(ChargeSuccessBean bean) {
        if (bean == null) {
            return;
        }
        if (mChargeAnimPresenter == null) {
            mChargeAnimPresenter = new ChargeAnimPresenter(mContext, mContentView);
        }
        if (mPaused) {
            mChargeAnimPresenter.save(bean);
        } else {
            mChargeAnimPresenter.showAnim(bean);
        }

    }

    @Override
    public void release() {
        EventBus.getDefault().unregister(this);
        MainHttpUtil.cancel(MainHttpConsts.GET_HOT);
        if (mChargeAnimPresenter != null) {
            mChargeAnimPresenter.release();
        }
        mChargeAnimPresenter = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        release();
    }



    @Override
    public void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPaused) {
            if (mChargeAnimPresenter != null) {
                ChargeSuccessBean bean = mChargeAnimPresenter.get();
                if (bean != null) {
                    mChargeAnimPresenter.showAnim(bean);
                }
            }
        }
        mPaused = false;
    }



    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_match_voice) {
            openMatchWindow(Constants.CHAT_TYPE_VOICE);
        } else if(i == R.id.btn_rank){
            RankListActivity.forward(mContext, 0);
        }
    }



    /**
     * 打开匹配弹窗
     *
     * @param type
     */
    private void openMatchWindow(int type) {
        MatchDialogFragment fragment = new MatchDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.CHAT_TYPE, type);
        fragment.setArguments(bundle);
        fragment.show(((AbsActivity) mContext).getSupportFragmentManager(), "MatchDialogFragment");
    }
}
