package com.yunbao.main.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;

import com.yunbao.common.Constants;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.utils.ClickUtil;
import com.yunbao.common.utils.DialogUitl;
import com.yunbao.common.utils.ToastUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;
import com.yunbao.main.bean.WallBean;
import com.yunbao.main.http.MainHttpUtil;
import com.yunbao.main.views.PhotoDetailViewHolder;

import java.util.ArrayList;
import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MyWallDetailActivity extends AbsActivity implements PhotoDetailViewHolder.ActionLister {

    public static void forward(Context context, WallBean bean, int wallImageSize, boolean isVideo) {
        if(!ClickUtil.canClick()){
            return;
        }
        Intent intent = new Intent(context, MyWallDetailActivity.class);
        intent.putExtra(Constants.WALL_BEAN, bean);
        intent.putExtra(Constants.WALL_IMAGE_SIZE, wallImageSize);
        intent.putExtra(Constants.WALL_IS_VIDEO, isVideo);
        context.startActivity(intent);
    }

    private WallBean mWallBean;
    private int mImageSize;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_empty;
    }

    @Override
    protected void main() {
        Intent intent = getIntent();
        mWallBean = intent.getParcelableExtra(Constants.WALL_BEAN);
        if (mWallBean == null) {
            return;
        }
        mImageSize = intent.getIntExtra(Constants.WALL_IMAGE_SIZE, 1);
        PhotoDetailViewHolder vh = new PhotoDetailViewHolder(mContext, (ViewGroup) findViewById(R.id.container), mWallBean.getThumb(), true);
        vh.setActionLister(this);
        vh.subscribeActivityLifeCycle();
        vh.addToParent();
    }

    @Override
    public void onMoreClick() {
        if (mWallBean == null) {
            return;
        }
        List<Integer> list = new ArrayList<>();
        list.add(R.string.wall_replace_img);
        if (mImageSize > 1) {
            list.add(R.string.delete);
        }
        DialogUitl.showStringArrayDialog(mContext, list.toArray(new Integer[list.size()]), true, new DialogUitl.StringArrayDialogCallback() {
            @Override
            public void onItemClick(String text, int tag) {
                if (tag == R.string.wall_replace_img) {
                    if (mWallBean != null) {
                        MyWallChooseImageActivity.forward(mContext, 1, mWallBean.getId());
                    }
                }else if (tag == R.string.delete) {
                    delete();
                }
            }
        });
    }


    /**
     * 删除自己的背景墙中的照片
     */
    private void delete() {
        DialogUitl.showSimpleDialog(mContext, WordUtil.getString(R.string.wall_delete_tip), new DialogUitl.SimpleCallback() {
            @Override
            public void onConfirmClick(Dialog dialog, String content) {
                if (mWallBean == null) {
                    return;
                }
                MainHttpUtil.deleteWall(mWallBean.getId(), new HttpCallback() {
                    @Override
                    public void onSuccess(int code, String msg, String[] info) {
                        if (code == 0) {
                            finish();
                        }
                        ToastUtil.show(msg);
                    }
                });
            }
        });
    }


}
