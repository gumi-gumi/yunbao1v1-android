package com.yunbao.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.adapter.RefreshAdapter;
import com.yunbao.common.bean.LevelBean;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.CommonIconUtil;
import com.yunbao.common.utils.L;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.main.R;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class SearchAdapter extends RefreshAdapter<UserBean> {

    private View.OnClickListener mClickListener;
    private String mIdString;
    private String mFanString;

    public SearchAdapter(Context context) {
        super(context);
        mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object tag = v.getTag();
                if (tag == null) {
                    return;
                }
                UserBean bean = (UserBean) tag;
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(bean, 0);
                }

            }
        };
        mIdString = WordUtil.getString(R.string.search_id);
        mFanString = WordUtil.getString(R.string.search_fans);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vh(mInflater.inflate(R.layout.item_search, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position) {
        ((Vh) vh).setData(mList.get(position));
    }


    class Vh extends RecyclerView.ViewHolder {

        ImageView mAvatar;
        TextView mName;
        ImageView mSex;
        TextView mID;
        TextView mFans;
        ImageView mHaveAuth;
        ImageView mNotAuth;

        public Vh(View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mSex = itemView.findViewById(R.id.sex);
            mID = itemView.findViewById(R.id.id_val);
            mFans = itemView.findViewById(R.id.fans);
            mHaveAuth = itemView.findViewById(R.id.have_auth);
            mNotAuth = itemView.findViewById(R.id.not_auth);
            itemView.setOnClickListener(mClickListener);
        }

        void setData(UserBean bean) {
            itemView.setTag(bean);
            ImgLoader.displayAvatar(mContext, bean.getAvatar(), mAvatar);
            mName.setText(bean.getUserNiceName());
            mSex.setImageResource(CommonIconUtil.getSexIcon(bean.getSex()));
            if (bean.getIsAuthorAuth() == 1) {

                mHaveAuth.setVisibility(View.VISIBLE);
                mNotAuth.setVisibility(View.GONE);
                mFans.setVisibility(View.VISIBLE);
                mFans.setText(StringUtil.contact(mFanString, StringUtil.toWan(bean.getFans())));
            } else {

                mHaveAuth.setVisibility(View.GONE);
                mNotAuth.setVisibility(View.VISIBLE);
                mFans.setVisibility(View.GONE);
            }

            mID.setText(StringUtil.contact(mIdString, bean.getId()));

        }

    }
}
