package com.yunbao.main.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.bean.UserBean;
import com.yunbao.common.http.HttpCallback;
import com.yunbao.common.http.HttpClient;
import com.yunbao.common.interfaces.CommonCallback;
import com.yunbao.common.utils.MD5Util;
import com.yunbao.common.utils.SpUtil;
import com.yunbao.common.utils.StringUtil;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-03-22
// +———————————————————————————————————
public class MainHttpUtil {
    /**
     * 取消网络请求
     */
    public static void cancel(String tag) {
        HttpClient.getInstance().cancel(tag);
    }

    /**
     * 手机号 验证码登录
     */
    public static void login(String phoneNum, String code,  HttpCallback callback) {
        HttpClient.getInstance().get("Login.userLogin", MainHttpConsts.LOGIN)
                .params("user_login", phoneNum)
                .params("code", code)
                .params("country_code", 86)
                .execute(callback);
    }


    /**
     * 获取登录信息，三方登录类型，服务和隐私条款
     */
    public static void getLoginInfo(HttpCallback callback) {
        HttpClient.getInstance().get("Login.GetLoginType", MainHttpConsts.GET_LOGIN_INFO)
                .execute(callback);
    }

    /**
     * 获取登录验证码接口
     */
    public static void getLoginCode(String phoneNum, HttpCallback callback) {
        String sign = MD5Util.getMD5(StringUtil.contact("mobile=", phoneNum, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Login.getCode", MainHttpConsts.GET_LOGIN_CODE)
                .params("mobile", phoneNum)
                .params("sign", sign)
                .params("country_code", 86)
                .execute(callback);
    }

    /**
     * 首页直播
     */
    public static void getHot(int p, byte sex, byte chatType, HttpCallback callback) {
        HttpClient.getInstance().get("Home.getHot", MainHttpConsts.GET_HOT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("sex", sex)
                .params("type", chatType)
                .params("p", p)
                .execute(callback);
    }


    /**
     * 首页 用户列表
     */
    public static void getHomeUserList(int type, int p, HttpCallback callback) {
        HttpClient.getInstance().get("User.getUserList", MainHttpConsts.GET_HOME_USER_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("type", type)
                .params("p", p)
                .execute(callback);
    }

    /**
     * 获取用户信息
     */
    public static void getBaseInfo(String uid, String token, final CommonCallback<Object[]> commonCallback) {
        HttpClient.getInstance().get("User.getBaseInfo", MainHttpConsts.GET_BASE_INFO)
                .params("uid", uid)
                .params("token", token)
                .execute(new HttpCallback() {
                    @Override
                    public void onSuccess(int code, String msg, String[] info) {
                        if (code == 0 && info.length > 0) {
                            JSONObject obj = JSON.parseObject(info[0]);
                            UserBean bean = JSON.toJavaObject(obj, UserBean.class);
                            CommonAppConfig appConfig = CommonAppConfig.getInstance();
                            appConfig.setUserBean(bean);
                            appConfig.setPriceVideo(obj.getString("video_value"));
                            appConfig.setUserSwitchVideo(obj.getIntValue("isvideo") == 1);
                            appConfig.setUserItemList(obj.getString("list"));
                            SpUtil.getInstance().setStringValue(SpUtil.USER_INFO, info[0]);
                            if (commonCallback != null) {
                                Object[] arr=new Object[2];
                                arr[0]=bean;
                                arr[1]=obj;
                                commonCallback.callback(arr);
                            }

                        }
                    }

                    @Override
                    public void onError() {
                        if (commonCallback != null) {
                            commonCallback.callback(null);
                        }
                    }
                });
    }


    /**
     * 获取用户信息
     */
    public static void getBaseInfo(CommonCallback<Object[]> commonCallback) {
        getBaseInfo(CommonAppConfig.getInstance().getUid(),
                CommonAppConfig.getInstance().getToken(),
                commonCallback);
    }


    /**
     * 搜索
     */
    public static void search(String key, int p, HttpCallback callback) {
        HttpClient.getInstance().get("Home.search", MainHttpConsts.SEARCH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("key", key)
                .params("p", p)
                .execute(callback);
    }

    /**
     * 获取对方的关注列表
     *
     * @param touid 对方的uid
     */
    public static void getFollowList(String touid, int p, HttpCallback callback) {
        HttpClient.getInstance().get("User.getFollowsList", MainHttpConsts.GET_FOLLOW_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("touid", touid)
                .params("p", p)
                .execute(callback);
    }

    /**
     * 获取对方的粉丝列表
     *
     * @param touid 对方的uid
     */
    public static void getFansList(String touid, int p, HttpCallback callback) {
        HttpClient.getInstance().get("User.getFansList", MainHttpConsts.GET_FANS_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("touid", touid)
                .params("p", p)
                .execute(callback);

    }


    /**
     * 更新用户资料
     *
     * @param avatar 头像
     * @param name   昵称
     */
    public static void updateUserInfo(String avatar,
                                      String name,
                                      int sex,
                                      String height,
                                      String weight,
                                      String constellation,
                                      HttpCallback callback) {
        HttpClient.getInstance().get("User.UpUserInfo", MainHttpConsts.UPDATE_USER_INFO)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("avatar", avatar)
                .params("name", name)
                .params("sex", sex)
                .params("height", height)
                .params("weight", weight)
                .params("constellation", constellation)
                .execute(callback);
    }


    /**
     * 获取 我的收益 可提现金额数
     */
    public static void getProfit(HttpCallback callback) {
        HttpClient.getInstance().get("Cash.getProfit", MainHttpConsts.GET_PROFIT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }

    /**
     * 获取 提现账户列表
     */
    public static void getCashAccountList(HttpCallback callback) {
        HttpClient.getInstance().get("Cash.GetUserAccountList", MainHttpConsts.GET_USER_ACCOUNT_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


    /**
     * 添加 提现账户
     */
    public static void addCashAccount(String account, String name, String bank, int type, HttpCallback callback) {
        HttpClient.getInstance().get("Cash.SetUserAccount", MainHttpConsts.ADD_CASH_ACCOUNT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("account", account)
                .params("name", name)
                .params("account_bank", bank)
                .params("type", type)
                .execute(callback);
    }

    /**
     * 删除 提现账户
     */
    public static void deleteCashAccount(String accountId, HttpCallback callback) {
        HttpClient.getInstance().get("Cash.delUserAccount", MainHttpConsts.DEL_CASH_ACCOUNT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("id", accountId)
                .execute(callback);
    }

    /**
     * 提现
     */
    public static void doCash(String votes, String accountId, HttpCallback callback) {
        HttpClient.getInstance().get("Cash.setCash", MainHttpConsts.DO_CASH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("cashvote", votes)//提现的票数
                .params("accountid", accountId)//账号ID
                .execute(callback);
    }

    /**
     * 视频接听开关 0关 1开
     *
     * @param open
     * @param callback
     */
    public static void setVideoSwitch(boolean open, HttpCallback callback) {
        HttpClient.getInstance().get("User.SetVideoSwitch", MainHttpConsts.SET_VIDEO_SWITCH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("isvideo", open ? 1 : 0)
                .execute(callback);
    }

    /**
     * 设置视频价格
     */
    public static void setVideoPrice(String value, HttpCallback callback) {
        HttpClient.getInstance().get("User.SetVideoValue", MainHttpConsts.SET_VIDEO_PRICE)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("value", value)
                .execute(callback);
    }


    /**
     * 礼物柜 获取礼物列表
     */
    public static void getGiftCabList(String liveUid, HttpCallback callback) {
        HttpClient.getInstance().get("User.GetGiftCab", MainHttpConsts.GET_GIFT_CAB_LIST)
                .params("liveuid", liveUid)
                .execute(callback);
    }

    /**
     * 获取自己的相册
     */
    public static void getMyAlbum(int p, String status, HttpCallback callback) {
        HttpClient.getInstance().get("Photo.MyPhoto", MainHttpConsts.GET_MY_ALBUM)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("p", p)
                .params("status", status)
                .execute(callback);
    }


    /**
     * 把相册图片信息保存在服务器
     */
    public static void setPhoto(String thumb, int isprivate, String coin, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String sign = MD5Util.getMD5(StringUtil.contact("isprivate=", String.valueOf(isprivate), "&thumb=", thumb, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Photo.SetPhoto", MainHttpConsts.SET_PHOTO)
                .params("uid", uid)
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("thumb", thumb)
                .params("isprivate", isprivate)
                .params("coin", coin)
                .params("sign", sign)
                .execute(callback);
    }

    /**
     * 删除自己的相册中的照片
     */
    public static void deletePhoto(String photoId, HttpCallback callback) {
        HttpClient.getInstance().get("Photo.DelPhoto", MainHttpConsts.DELETE_PHOTO)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("photoid", photoId)
                .execute(callback);
    }



    /**
     * 设置背景墙的封面图
     */
    public static void setWallCover(String thumb, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String sign = MD5Util.getMD5(StringUtil.contact("thumb=", thumb, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Wall.SetCover", MainHttpConsts.SET_WALL_COVER)
                .params("uid", uid)
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("thumb", thumb)
                .params("sign", sign)
                .execute(callback);
    }


    /**
     * 获取个人主页相册
     */
    public static void getHomePhoto(String liveuid, int p, HttpCallback callback) {
        HttpClient.getInstance().get("Photo.GetHomePhoto", MainHttpConsts.GET_HOME_PHOTO)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("liveuid", liveuid)
                .params("p", p)
                .execute(callback);
    }

    /**
     * 用于更新照片观看量
     */
    public static void photoAddView(String photoId, HttpCallback callback) {
        String uid = CommonAppConfig.getInstance().getUid();
        String sign = MD5Util.getMD5(StringUtil.contact("photoid=", photoId, "&uid=", uid, "&", HttpClient.SALT));
        HttpClient.getInstance().get("Photo.AddView", MainHttpConsts.PHOTO_ADD_VIEW)
                .params("uid", uid)
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("photoid", photoId)
                .params("sign", sign)
                .execute(callback);
    }


    /**
     * 获取我的背景墙
     */
    public static void getMyWall(HttpCallback callback) {
        HttpClient.getInstance().get("Wall.MyWall", MainHttpConsts.GET_MY_WALL)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


    /**
     * 删除自己的背景墙中的照片
     */
    public static void deleteWall(String wallId, HttpCallback callback) {
        HttpClient.getInstance().get("Wall.DelWall", MainHttpConsts.DELETE_WALL)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("wallid", wallId)
                .execute(callback);
    }


    /**
     * 设置背景墙中的照片
     *
     * @param action 行为,0新加,1替换
     * @param type   类型，0图片1视频
     * @param oldId  旧背景ID
     * @param newId  新背景ID
     */
    public static void setWall(int action, int type, String oldId, String newId, HttpCallback callback) {
        HttpClient.getInstance().get("Wall.SetWall", MainHttpConsts.SET_WALL)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("action", action)
                .params("type", type)
                .params("oldid", oldId)
                .params("newid", newId)
                .execute(callback);
    }

    /**
     * 检查是否要弹邀请码的弹窗
     */
    public static void checkAgent(HttpCallback callback) {
        HttpClient.getInstance().get("Agent.Check", MainHttpConsts.CHECK_AGENT)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }



    //排行榜  收益榜
    public static void profitList(String type, int p, HttpCallback callback) {
        HttpClient.getInstance().get("Home.profitList", MainHttpConsts.PROFIT_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("type", type)
                .params("p", p)
                .execute(callback);
    }

    //排行榜  贡献榜
    public static void consumeList(String type, int p, HttpCallback callback) {
        HttpClient.getInstance().get("Home.consumeList", MainHttpConsts.CONSUME_LIST)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("type", type)
                .params("p", p)
                .execute(callback);

    }

    /**
     * 设置用户信息
     */
    public static void setUserProfile(String avatar, String name, int sex, HttpCallback callback) {
        HttpClient.getInstance().get("User.setUserInfo", MainHttpConsts.SET_USER_PROFILE)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("avatar", avatar)
                .params("name", name)
                .params("sex", sex)
                .execute(callback);
    }

    /**
     * 获取用户实名认证和主播认证状态
     */
    public static void getAuthStatus(HttpCallback callback) {
        HttpClient.getInstance().get("Auth.getAuthStatus", MainHttpConsts.GET_AUTH_STATUS)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }

    /**
     * 用户实名认证
     */
    public static void setUserAuth(String name,String mobile,String cardNo,HttpCallback callback) {
        HttpClient.getInstance().get("Auth.setUserAuth", MainHttpConsts.SET_USER_AUTH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .params("name", name)
                .params("mobile", mobile)
                .params("cardno", cardNo)
                .execute(callback);
    }

    /**
     * 获取用户基本信息
     */
    public static void getUserMaterial(HttpCallback callback) {
        HttpClient.getInstance().get("User.getUserMaterial", MainHttpConsts.SET_USER_AUTH)
                .params("uid", CommonAppConfig.getInstance().getUid())
                .params("token", CommonAppConfig.getInstance().getToken())
                .execute(callback);
    }


}




