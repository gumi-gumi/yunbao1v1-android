
![输入图片说明](/img/head.jpg)
### 项目资料、说明
开源源码：原生开发，可下载使用【本仓库为安卓端】【[点击前往web端](https://gitee.com/yunbaokeji/yunbao1v1-web)】

搭建文档：辅助操作，程序可跑通【[点击查看后台搭建文档](https://shimo.im/docs/WQxPOFDAHlo5e5MD/)】【[点击查看安卓搭建文档](https://shimo.im/docs/azHGDI7uLWI9s2pj/)】

疑难辅助说明：常见问题解释，答疑解惑【见搭建文档最下方说明和本项目的下方评论】

查看演示：

后台地址：http://git1v1.yunbaozb.com/admin   

后台用户名：admin    密码：visitor

安卓扫描下载：

![输入图片说明](/img/qr.png)


### 项目介绍

本开源项目由云豹科技官方自主研发，提供开源源码、搭建文档、疑难辅助说明，供大家参考、交流、使用。 开源版程序主要围绕“一对一直播功能”展开，如您需要更多专业功能、优质服务，可联系我们了解商用版本、运营版本。

### 联系我们

公司官网：http://www.yunbaokj.com/

客服电话：17662585037

![输入图片说明](/img/contact.png)

### 云豹科技介绍

![输入图片说明](/img/about.jpg)

一对一直播源码|一对一视频直播源码|一对一视频app源码|一对一视频直播app源码|一对一社交源码